# DireDog
## Milestone 4: Team Project
### Requirements
* Database: Relational tables and roles
    * Separate tables for users, club organizers
    * Users and clubs are connected through Memberships
    * Users have multiple cars, connected through Garage
* Web interface 
    * Main hub with views of different clubs
    * Each club page has:
        * About / General info
        * Events / schedule page
        * Registration material
        * Club organizers
    * All users will have to register with the website so their information can be linked to the database
* NHTSA API Integration
    * API to be used to retrieve information on cars entered by the user so they can register them to their account
    * Allows listing of parts on a car so that users can list modifications
    * The API will allow the website to suggest or show users that have similar car set ups
    * API to help the user determine which competetive classes they can/should enroll their cars in.
* QR Code for user information
    * Scannable by event organizers to retrieve registration status of participant
        