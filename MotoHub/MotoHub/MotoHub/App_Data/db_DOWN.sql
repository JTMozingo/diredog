IF EXISTS
(
	SELECT *
	FROM sys.tables
	WHERE tables.name IN ('AspNetUsers','AspNetRoles','AspNetUserClaims', 
	'AspNetUserLogins', 'AspNetUserRoles', 'MotoHubUsers', 'UserProfile', 
	'Clubs', 'Membership', 'MotoHubRoles','Events', 'MotoHubUsers', 'Photos', 'PhotoAlbums', 
	'Gallery', 'Garage', 'Car','CarMakes','CarColor','CarType','CarClass', 'RunSection', 'RunGroup')
)

BEGIN
	DROP TABLE dbo.AspNetUserClaims
	DROP TABLE dbo.AspNetUserLogins
	DROP TABLE dbo.AspNetUserRoles
	DROP TABLE dbo.AspNetRoles
	DROP TABLE dbo.AspNetUsers

	DROP TABLE dbo.PendingClubInvitations
	DROP TABLE dbo.Membership
	DROP TABLE dbo.Registration
	DROP TABLE dbo.Events
	DROP TABLE dbo.RunData
	DROP TABLE dbo.EventResults
	DROP TABLE dbo.Committee
	DROP TABLE dbo.Clubs
	DROP TABLE dbo.UserProfile
	DROP TABLE dbo.Photos
	DROP TABLE dbo.PhotoAlbums
	DROP TABLE dbo.MotoHubRoles
    
	DROP TABLE dbo.Car
	DROP TABLE dbo.CarMakes
	DROP TABLE dbo.CarColor
	DROP TABLE dbo.CarType
	DROP TABLE dbo.CarClass
	DROP TABLE dbo.Garage
    
	DROP TABLE dbo.MotoHubUsers
	DROP TABLE dbo.Gallery

	DROP TABLE dbo.RunGroup
	DROP TABLE dbo.RunSection
END