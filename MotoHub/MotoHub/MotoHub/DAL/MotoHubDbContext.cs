namespace MotoHub.DAL
{
    using System;
    using MotoHub.Models;
    using System.Data.Entity;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;

    public partial class MotoHubDbContext : DbContext, IMotoHubDbContext
    {
        public MotoHubDbContext()
            : base("name=MotoHubDbContext")
        {
        }
        public virtual DbSet<Car> Cars { get; set; }
        public virtual DbSet<CarClass> CarClasses { get; set; }
        public virtual DbSet<CarColor> CarColors { get; set; }
        public virtual DbSet<CarMake> CarMakes { get; set; }
        public virtual DbSet<CarType> CarTypes { get; set; }
        public virtual DbSet<Club> Clubs { get; set; }
        public virtual DbSet<Committee> Committees { get; set; }
        public virtual DbSet<EventResult> EventResults { get; set; }
        public virtual DbSet<Event> Events { get; set; }
        public virtual DbSet<Gallery> Galleries { get; set; }
        public virtual DbSet<Garage> Garages { get; set; }
        public virtual DbSet<Membership> Memberships { get; set; }
        public virtual DbSet<MotoHubRole> MotoHubRoles { get; set; }
        public virtual DbSet<MotoHubUser> MotoHubUsers { get; set; }
        public virtual DbSet<PhotoAlbum> PhotoAlbums { get; set; }
        public virtual DbSet<Photo> Photos { get; set; }
        public virtual DbSet<Registration> Registrations { get; set; }
        public virtual DbSet<RunData> RunDatas { get; set; }
        public virtual DbSet<UserProfile> UserProfiles { get; set; }
        public virtual DbSet<RunSection> RunSections { get; set; }
        public virtual DbSet<RunGroup> RunGroups { get; set; }
        public virtual DbSet<PendingClubInvitation> PendingClubInvitations { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
           
            modelBuilder.Entity<CarMake>()
                .HasMany(e => e.Cars)
                .WithRequired(e => e.CarMake)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Club>()
                .HasMany(e => e.Events)
                .WithRequired(e => e.Club)
                .HasForeignKey(e => e.ClubID);

            modelBuilder.Entity<Club>()
                .HasMany(e => e.Memberships)
                .WithRequired(e => e.Club)
                .HasForeignKey(e => e.ClubID);

            modelBuilder.Entity<Committee>()
                .HasMany(e => e.Memberships)
                .WithOptional(e => e.Committee)
                .WillCascadeOnDelete();

            modelBuilder.Entity<EventResult>()
                .HasMany(e => e.RunDatas)
                .WithRequired(e => e.EventResult)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Event>()
                .HasMany(e => e.Registrations)
                .WithRequired(e => e.Event)
                .HasForeignKey(e => e.EventID)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Gallery>()
                .HasMany(e => e.Events)
                .WithRequired(e => e.Gallery)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Gallery>()
                .HasMany(e => e.PhotoAlbums)
                .WithRequired(e => e.Gallery)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Gallery>()
                .HasMany(e => e.UserProfiles)
                .WithRequired(e => e.Gallery)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Garage>()
                .HasMany(e => e.Cars)
                .WithRequired(e => e.Garage)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<MotoHubUser>()
                .HasMany(e => e.Registrations)
                .WithRequired(e => e.MotoHubUser)
                .HasForeignKey(e => e.UserID)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<MotoHubUser>()
                .HasOptional(e => e.UserProfile)
                .WithRequired(e => e.MotoHubUser)
                .WillCascadeOnDelete();

            modelBuilder.Entity<PhotoAlbum>()
                .HasMany(e => e.Photos)
                .WithRequired(e => e.PhotoAlbum)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Photo>()
                .HasMany(e => e.Clubs)
                .WithOptional(e => e.Photo)
                .HasForeignKey(e => e.ProfilePhotoID)
                .WillCascadeOnDelete();

            modelBuilder.Entity<Photo>()
                .HasMany(e => e.Events)
                .WithOptional(e => e.Photo)
                .HasForeignKey(e => e.ProfilePhotoID);

            modelBuilder.Entity<Photo>()
                .HasMany(e => e.UserProfiles)
                .WithOptional(e => e.Photo)
                .HasForeignKey(e => e.ProfilePhotoID)
                .WillCascadeOnDelete();

            modelBuilder.Entity<Registration>()
                .HasMany(e => e.EventResults)
                .WithRequired(e => e.Registration)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<UserProfile>()
                .HasMany(e => e.PendingClubInvitations)
                .WithRequired(e => e.UserProfile)
                .WillCascadeOnDelete(false);
        
        }

        public void MarkAsModified(Club item)
        {
            Entry(item).State = EntityState.Modified;
        }
    }
}