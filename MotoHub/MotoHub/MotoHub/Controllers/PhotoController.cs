﻿using MotoHub.DAL;
using MotoHub.Models;
using MotoHub.Models.ViewModels;
using System.Data.Entity;
using System;
using System.Linq;
using System.Web;
using MotoHub.MotoHubDevLibrary;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using System.Collections.Generic;
using MotoHub.Repositories.RepositoryInterfaces;

namespace MotoHub.Controllers
{
    public class PhotoController : Controller
    {
        private MotoHubDbContext db = new MotoHubDbContext();
        private DevTools devTools = new DevTools();
        private IPhotosRepository _photos;

        public PhotoController(IPhotosRepository photoRepo)
        {
            _photos = photoRepo;
        }

        public ActionResult EditPhotos(int? galleryID)
        {
            PhotoAlbumViewModel model = CreatePhotoAlbumViewModel(galleryID);

            return View(model);
        }

        public ActionResult GetPhotoEditPage(int? galleryID)
        {
            if(galleryID == null || galleryID <= 0)
            {
                throw new HttpException("The gallery you are looking for does not exist. HttpRequest galleryID is null or 0.");

            }
            else
            {
                return RedirectToAction("EditPhotos", new { galleryID });
            }
        }

        /// <summary>
        /// Create a PhotoAlbumViewModel based on a certian gallery.
        /// </summary>
        /// <param name="galleryID">Gallery to relate all PhotoAlbumViewModel models to.</param>
        /// <returns>PhotoAlbumViewModel</returns>
        private PhotoAlbumViewModel CreatePhotoAlbumViewModel(int? galleryID)
        {

            PhotoAlbumViewModel photoAlbumViewModel = new PhotoAlbumViewModel
            {
                Gallery = db.Galleries.Find(galleryID),
                PhotoAlbums = GetGalleryPhotoAlbums(galleryID),
                Photos = GetGalleryPhotos(galleryID)
            };

            return photoAlbumViewModel;
        }

        /// <summary>
        /// Return all of the PhotoAlbums associated with a particular gallery table entry.
        /// </summary>
        /// <param name="galleryID">The gallery to search in.</param>
        /// <returns>IEnmumerable<PhotoAlbums> list of PhotoAlbums</returns>
        private IEnumerable<PhotoAlbum> GetGalleryPhotoAlbums(int? galleryID)
        {
            List<PhotoAlbum> photoAlbums = db.PhotoAlbums.Where(pa => pa.GalleryID == galleryID).ToList();

            return photoAlbums;
        }

        /// <summary>
        /// Returns all of the Photos associated with a particular gallery table entry.
        /// </summary>
        /// <param name="galleryID">The gallery to search in.</param>
        /// <returns>IEnmumerable<Photo> list of Photos</returns>
        private IEnumerable<Photo> GetGalleryPhotos(int? galleryID)
        {
            List<Photo> photos = db.Photos.Where(p => p.PhotoAlbumID == db.PhotoAlbums
                                          .Where(pa => pa.GalleryID == galleryID)
                                          .Select(pa => pa.PhotoAlbumID)
                                          .FirstOrDefault())
                                          .ToList();
            return photos;
        }

        /// <summary>
        /// Returns a gallery with the matching galleryID input into the method.
        /// </summary>
        /// <param name="galleryID">Gallery to search for</param>
        /// <returns>A gallery table object</returns>
        private Gallery GetGallery(int? galleryID)
        {
            Gallery gallery = db.Galleries.Find(galleryID);

            return gallery;
        }

        /// <summary>
        /// Processes a request from the client to add a photo to a specified Photo Album.
        /// This will create the new Photo entry and update the database.
        /// </summary>
        /// <returns>Json result of success or failure.</returns>
        [HttpPost]
        public JsonResult AddPhotoToAlbum()
        {

            if(Request.Files.Keys.Count > 0)
            {
                //Get all of the keys from the HttpRequest.
                var keys = Request.Form.AllKeys;
                //Get string representation of AlbumID
                var stringAlbumID = Request.Form.Get(keys[0]);
                //Convert to int.
                Int32.TryParse(stringAlbumID, out int albumID);

                //Get a collection of all the files in the HttpRequest
                HttpFileCollectionBase fileCollection = Request.Files;
                //Get the file that is stored away.
                HttpPostedFileBase file = fileCollection[0];

                Photo newPhoto = new Photo
                {
                    PhotoName = file.FileName,
                    ContentType = file.ContentType,
                    PhotoData = new byte[file.ContentLength],
                    PhotoAlbumID = albumID,
                };
                //Create the byte stream of the photo.
                file.InputStream.Read(newPhoto.PhotoData, 0, file.ContentLength);

                db.Photos.Add(newPhoto);
                db.SaveChanges();

                return Json("Successfully Uploaded File");
            }
            else
            {
                return Json("No Files Selected.");
            }

        }

        public FileContentResult GetPhotoAsFile(int photoID)
        {
            Photo image = GetPhoto(photoID);

            return File(image.PhotoData, image.ContentType);
        }

        /// <summary>
        /// Gets the file of a photo based on the passed ID.
        /// </summary>
        /// <param name="photoID">ID of photo to return.</param>
        /// <returns>File format of the photo.</returns>
        public Photo GetPhoto(int photoID)
        {
            Photo image = db.Photos.Find(photoID);

            return image;
        }

        public JsonResult Edit([Bind(Prefix = "item")]Photo photo)
        {

            Photo photoToUpdate = db.Photos.Find(photo.PhotoID);

            photoToUpdate.PhotoCaption = photo.PhotoCaption;

            db.Entry(photoToUpdate).State = EntityState.Modified;
            db.SaveChanges();

            return Json("Edited");
        }

        [Authorize]
        public ActionResult DeletePhoto(int photoID)
        {
            //Check to see if this photo is a profile photo somewhere before removing it.
            Photo photoToRemove = db.Photos.Find(photoID);
            UserProfile profile = db.UserProfiles.Where(up => up.ProfilePhotoID == photoID).FirstOrDefault();
            Event @event = db.Events.Where(e => e.ProfilePhotoID == photoID).FirstOrDefault();
            Club club = db.Clubs.Where(c => c.ProfilePhotoID == photoID).FirstOrDefault();
            int galleryID = photoToRemove.PhotoAlbum.GalleryID;

            if(profile != null)
            {
                profile.ProfilePhotoID = null;
                profile.Photo = null;
                db.Entry(profile).State = EntityState.Modified;
                db.SaveChanges();
            }
            else if(@event != null)
            {
                @event.ProfilePhotoID = null;
                @event.Photo = null;
                db.Entry(@event).State = EntityState.Modified;
                db.SaveChanges();
            }
            else if (club != null)
            {
                club.ProfilePhotoID = null;
                club.Photo = null;
                db.Entry(club).State = EntityState.Modified;
                db.SaveChanges();
            }

            db.Photos.Remove(photoToRemove);
            db.SaveChanges();

            return RedirectToAction("EditPhotos", new { galleryID });
        }

        /// <summary>
        /// This will determine, based on the passed in galleryID, what table should be updated with a new photoID for
        /// it's ProfilePhotoID row.
        /// </summary>
        /// <param name="galleryID">The gallery the update should happen in.</param>
        /// <param name="photoID">The photo that will become the new profile photo.</param>
        /// <returns>View of the EditPhotos page.</returns>
        public ActionResult SetProfilePhoto(int? galleryID, int? photoID)
        {
            if(galleryID == null || galleryID <= 0)
            {
                throw new HttpException("The gallery recieved from the HttpRequest is either null or 0.");
            }
            else if(photoID == null || photoID <= 0)
            {
                throw new HttpException("The photo recieved from the HttpRequest is either null or 0.");
            }
            else
            {
                Gallery galleryFound = GetGallery(galleryID);

                if(galleryFound.UserProfiles.Count() > 0)
                {
                    AssignUserProfilePhoto(photoID, galleryFound.UserProfiles.Select(up => up.MotoHubUserID).FirstOrDefault());
                }
                else if(galleryFound.Clubs.Count() > 0)
                {
                    AssignClubProfilePhoto(photoID, galleryFound.Clubs.Select(c => c.ClubsID).FirstOrDefault());
                }
                else if(galleryFound.Events.Count() > 0)
                {
                    AssignEventProfilePhoto(photoID, galleryFound.Events.Select(e => e.EventsID).FirstOrDefault());
                }
                else
                {
                    throw new Exception("This gallery is an orphan. No Clubs, no UserProfile and no Events are associated with this Gallery.");
                };

                return RedirectToAction("EditPhotos", new { galleryID }); 
            }

        }

        /// <summary>
        /// Assigns a new ProfilePhotoID to the user.
        /// </summary>
        /// <param name="photoID">PhotoID of the new profile photo.</param>
        /// <param name="userID">MotoHubUserID of the user to update.</param>
        public void AssignUserProfilePhoto(int? photoID, int? userID)
        {
            if(photoID == null || photoID <= 0)
            {
                throw new Exception("Input photoID was null or 0.");
            }
            else if (userID == null || userID <= 0)
            {
                throw new Exception("Input userID was less than or equal to 0.");
            }
            else
            {
                UserProfile userProfileToUpdate = db.UserProfiles.Find(userID);

                userProfileToUpdate.ProfilePhotoID = photoID;

                db.Entry(userProfileToUpdate).State = EntityState.Modified;
                db.SaveChanges();
            }
        }

        /// <summary>
        /// Assigns a new ProfilePhotoID to the club.
        /// </summary>
        /// <param name="photoID">PhotoID of the new profile photo.</param>
        /// <param name="clubID">ClubsID of the club to update.</param>
        public void AssignClubProfilePhoto(int? photoID, int? clubID)
        {
            if(photoID == null || photoID <= 0)
            {
                throw new Exception("Input photoID was null or 0.");
            }
            else if(clubID == null || clubID <= 0)
            {
                throw new Exception("Input clubID was null or 0.");
            }
            else
            {
                Club clubToUpdate = db.Clubs.Find(clubID);

                clubToUpdate.ProfilePhotoID = photoID;

                db.Entry(clubToUpdate).State = EntityState.Modified;
                db.SaveChanges();
            }
        }

        /// <summary>
        /// Assigns a new ProfilePhotoID to the event.
        /// </summary>
        /// <param name="photoID">PhotoID of the new profile photo.</param>
        /// <param name="eventID">EventsID of the Event to update.</param>
        public void AssignEventProfilePhoto(int? photoID, int? eventID)
        {
            if(photoID == null || photoID <= 0)
            {
                throw new Exception("Input photoID was null or 0.");
            }
            else if(eventID == null || eventID <= 0)
            {
                throw new Exception("Input eventID was null or 0.");
            }
            else
            {
                Event eventToUpdate = db.Events.Find(eventID);

                eventToUpdate.ProfilePhotoID = photoID;

                db.Entry(eventToUpdate).State = EntityState.Modified;
                db.SaveChanges();
            }
        }
    }
}