﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using MotoHub.DAL;
using MotoHub.Models;
using MotoHub.MotoHubDevLibrary;
using GuigleAPI;
using GuigleAPI.Model;
using Microsoft.AspNet.Identity;
using MotoHub.Models.ViewModels;
using MotoHub.Repositories.Persistance;
using MotoHub.Repositories.RepositoryInterfaces;
using MotoHub.Repositories;

namespace MotoHub.Controllers
{
    public class EventsController : Controller
    {
        private MotoHubDbContext db = new MotoHubDbContext();
        private DevTools devTools = new DevTools();
        private UnitOfWork unitOfWork = new UnitOfWork(new MotoHubDbContext());
        private IEventRepository _events;
        private IClubRepository _clubs;
        private IMembershipRepository _memberships;
        private IRoleRepository _roles;
        private IMotoHubUserRepository _motoHubUsers;
        private IRunDataRepository _runDatas;
        private IEventResultsRepository _eventResults;
        private IRegistrationRepository _registrations;
        private IGalleriesRepository _galleries;
        private IPhotoAlbumsRepository _photoAlbums;
        private IPhotosRepository _photos;
        private IGarageRepository _garage;
        private IRunGroupRepository _runGroup;
        private IRunSectionRepository _runSection;

        /// <summary>
        /// Constructor for all repositories to be accessed on the live site.
        /// </summary>
        /// <param name="eventRepository"></param>
        /// <param name="clubRepo"></param>
        /// <param name="memRepo"></param>
        /// <param name="roleRepository"></param>
        /// <param name="userRepo"></param>
        /// <param name="runDataRepo"></param>
        public EventsController(IEventRepository eventRepository, IClubRepository clubRepo, IMembershipRepository memRepo, IRoleRepository roleRepository, IMotoHubUserRepository userRepo, IRunDataRepository runDataRepo, IEventResultsRepository eventResultsRepo, IRegistrationRepository registrationRepo, IGalleriesRepository galleriesRepo, IPhotoAlbumsRepository photoAlbumsRepo, IPhotosRepository photosRepo, IGarageRepository garageRepo, IRunGroupRepository runGroupRepo, IRunSectionRepository runSectionRepo)
        {
            _events = eventRepository;
            _clubs = clubRepo;
            _memberships = memRepo;
            _roles = roleRepository;
            _motoHubUsers = userRepo;
            _runDatas = runDataRepo;
            _eventResults = eventResultsRepo;
            _registrations = registrationRepo;
            _galleries = galleriesRepo;
            _photoAlbums = photoAlbumsRepo;
            _photos = photosRepo;
            _garage = garageRepo;
            _runGroup = runGroupRepo;
            _runSection = runSectionRepo;
        }

        /// <summary>
        /// Constuctor for specific repos pertaining to Moq testings.
        /// </summary>
        /// <param name="mockClubRepo"></param>
        /// <param name="mockEventRepo"></param>
        /// <param name="mockUserRepo"></param>
        /// <param name="mockMembershipRepo"></param>
        /// <param name="mockRoleRepo"></param>
        public EventsController(IClubRepository mockClubRepo, IEventRepository mockEventRepo, IMotoHubUserRepository mockUserRepo, IMembershipRepository mockMembershipRepo, IRoleRepository mockRoleRepo)
        {
            _events = mockEventRepo;
            _clubs = mockClubRepo;
            _memberships = mockMembershipRepo;
            _roles = mockRoleRepo;
            _motoHubUsers = mockUserRepo;
        }

        public bool MoqCreatedSuccessfully()
        {
            if (this._events.GetAll().Count() <= 0)
            {
                return false;
            }
            else
            {
                return true;
            }

        }

        public ActionResult EventResults(int eventID)
        {
            return View(_events.Get(eventID));
        }

        public ActionResult EditEventResults(int runDataID, int eventResultsID)
        {
            IEnumerable<RunData> runDataToEdit = _runDatas.GetAll().Where(rd => rd.RunDataID == runDataID);
            IEnumerable<EventResult> eventResultToEdit = _eventResults.GetAll().Where(er => er.EventResultsID == eventResultsID);

            EventResultsViewModel eventResultsViewModel = CreateEventResultsViewModel(eventResultToEdit, runDataToEdit);

            return View(eventResultsViewModel);
        }

        [HttpPost]
        public ActionResult EditEventResults([Bind(Prefix = "eventResultItem")] EventResult eventResult, [Bind(Prefix = "runDataItem")] RunData runData)
        {
            EventResult eventResultToEdit = _eventResults.Get(eventResult.EventResultsID);
            RunData runDataToEdit = _runDatas.Get(runData.RunDataID);

            eventResultToEdit.Points = eventResult.Points;
            eventResultToEdit.RunGroupCode = eventResult.RunGroupCode;
            eventResult.RunSectionCode = eventResult.RunSectionCode;

            runDataToEdit.RawTime = runData.RawTime;
            runDataToEdit.AdjustedTime = runData.AdjustedTime;
            runDataToEdit.Finished = runData.Finished;
            runDataToEdit.RunNumber = runData.RunNumber;
            runDataToEdit.Penalty = runData.Penalty;

            _runDatas.SetModified(runDataToEdit);
            _eventResults.SetModified(eventResultToEdit);

            _eventResults.SaveChanges();
            _runDatas.SaveChanges();

            EventResult test = _eventResults.Get(eventResult.EventResultsID);
            return RedirectToAction("EventResults", new { eventID = eventResultToEdit.Registration.EventID });
        }

        public ActionResult CreateNewRunData(int eventID)
        {
            return View(_events.Get(eventID));
        }
        
        /// <summary>
        /// Gets all of the EventResult table entries associated with an event.
        /// </summary>
        /// <param name="eventID">Event to look in.</param>
        /// <returns>List of EventResults table entries.</returns>
        private IEnumerable<EventResult> GetAllEventResults(int eventID)
        {
            return _eventResults.GetAll().Where(er => er.Registration.EventID == eventID).ToList();
        }

        private IEnumerable<EventResult> GetAllUserEventResults(int userID)
        {
            return _eventResults.GetAll().Where(er => er.Registration.UserID == userID).ToList();
        }

        public PartialViewResult GetAllUserPastEventResults()
        {
            int userID = _motoHubUsers.GetCurrentUser(User.Identity.GetUserId()).MotoHubUserID;

            IEnumerable<EventResult> allUserEventResults = GetAllUserEventResults(userID);

            List<EventResult> pastEventResults = GetAllPastEventResults(allUserEventResults);

            IEnumerable<RunData> allUserRunDataFromPastEventResults = _runDatas.GetAllRunDataFromEventResults(pastEventResults);

            EventResultsViewModel pastEventResultsViewModel = CreateEventResultsViewModel(pastEventResults, allUserRunDataFromPastEventResults);

            return PartialView("_UserEventResultsPartialView", pastEventResultsViewModel);
        }

        /// <summary>
        /// Gets all of the EventResults for the current user within the specified time range given.
        /// </summary>
        /// <param name="startDate">Start of the time range.</param>
        /// <param name="endDate">End of the time range.</param>
        /// <returns>Partial View of a table containing the results.</returns>
        public PartialViewResult GetUserEventResultsInDateRange(DateTime startDate, DateTime endDate)
        {
            int userID = _motoHubUsers.GetCurrentUser(User.Identity.GetUserId()).MotoHubUserID;

            IEnumerable<EventResult> allUserEventResults = GetAllUserEventResults(userID);
            List<EventResult> pastEventResults = new List<EventResult>();

            foreach (var eventResult in allUserEventResults)
            {
                if (eventResult.Registration.Event.EventDate < endDate && eventResult.Registration.Event.EventDate > startDate)
                {
                    pastEventResults.Add(eventResult);
                }
            }

            IEnumerable<RunData> allUserRunDataFromPastEventResults = _runDatas.GetAllRunDataFromEventResults(pastEventResults);

            EventResultsViewModel pastEventResultsViewModel = CreateEventResultsViewModel(pastEventResults, allUserRunDataFromPastEventResults);

            return PartialView("_UserEventResultsPartialView", pastEventResultsViewModel);
        }

        /// <summary>
        /// Gets the most recent 
        /// </summary>
        /// <param name="numberOfResults"></param>
        /// <returns></returns>
        public PartialViewResult GetSpecifiedNumberOfMostRecentUserEventResults(int numberOfResults)
        {
            int userID = _motoHubUsers.GetCurrentUser(User.Identity.GetUserId()).MotoHubUserID;

            IEnumerable<EventResult> allUserEventResults = GetAllUserEventResults(userID);

            List<EventResult> pastEventResults = GetAllPastEventResults(allUserEventResults);

            pastEventResults = pastEventResults.OrderByDescending(eventResult => eventResult.Registration.Event.EventDate).ToList();
            pastEventResults = pastEventResults.Take(numberOfResults).ToList();

            IEnumerable<RunData> allUserRunDataFromPastEventResults = _runDatas.GetAllRunDataFromEventResults(pastEventResults);

            EventResultsViewModel pastEventResultsViewModel = CreateEventResultsViewModel(pastEventResults, allUserRunDataFromPastEventResults);

            return PartialView("_UserEventResultsPartialView", pastEventResultsViewModel);
        }

        /// <summary>
        /// Helper method to get all the past events based on the current system time.
        /// </summary>
        /// <param name="eventResults">Events list to filter</param>
        /// <returns>All events in the parameter list that have a DateTime of before System.DateTime.Now</returns>
        private List<EventResult> GetAllPastEventResults(IEnumerable<EventResult> eventResults)
        {
            List<EventResult> pastEventResults = new List<EventResult>();

            foreach (var eventResult in eventResults)
            {
                if (eventResult.Registration.Event.EventDate < System.DateTime.Now)
                {
                    pastEventResults.Add(eventResult);
                }
            }

            return pastEventResults;
        }

        [HttpPost]
        public PartialViewResult EventResults(int eventID, string runGroup, string runSection)
        {
            EventResultsViewModel eventResultsViewModel = new EventResultsViewModel();

            bool filterByRunGroup = _runGroup.GetRunGroup(runGroup) != null;
            bool filterByRunSection = _runSection.GetRunSection(runSection) != null;

            //Filtered by Group
            if (filterByRunGroup && !filterByRunSection)
            {
                IEnumerable<EventResult> filteredEventResultsByGroup = GetAllEventResultsWithGroup(runGroup, eventID);
                IEnumerable<RunData> eventRunData = _runDatas.GetAllRunDataFromEventResults(filteredEventResultsByGroup);

                eventResultsViewModel = CreateEventResultsViewModel(filteredEventResultsByGroup, eventRunData);

                return PartialView("_EventResultsTable", eventResultsViewModel);
            }
            //Filtered By Section
            else if (!filterByRunGroup && filterByRunSection)
            {
                IEnumerable<EventResult> filteredEventResultsBySection = GetAllEventResultsWithSection(runSection, eventID);
                IEnumerable<RunData> eventRunData = _runDatas.GetAllRunDataFromEventResults(filteredEventResultsBySection);

                eventResultsViewModel = CreateEventResultsViewModel(filteredEventResultsBySection, eventRunData);

                return PartialView("_EventResultsTable", eventResultsViewModel);
            }
            //Filtered By Both
            else if (filterByRunGroup && filterByRunSection)
            {
                IEnumerable<EventResult> filteredEventResultsByGroup = GetAllEventResultsWithGroup(runGroup, eventID);
                IEnumerable<EventResult> filteredEventResultsBySection = GetAllEventResultsWithSection(runSection, eventID);
                IEnumerable<EventResult> filteredEventResultsByGroupAndBySection = filteredEventResultsByGroup.Intersect(filteredEventResultsBySection);

                IEnumerable<RunData> eventRunData = _runDatas.GetAllRunDataFromEventResults(filteredEventResultsByGroupAndBySection);

                eventResultsViewModel = CreateEventResultsViewModel(filteredEventResultsByGroupAndBySection, eventRunData);

                return PartialView("_EventResultsTable", eventResultsViewModel);
            }
            IEnumerable<EventResult> filteredEventResults = GetAllEventResults(eventID);
            IEnumerable<RunData> filteredRunData = _runDatas.GetAllRunDataFromEventResults(filteredEventResults);

            eventResultsViewModel = CreateEventResultsViewModel(filteredEventResults, filteredRunData);

            return PartialView("_EventResultsTable", eventResultsViewModel);
        }

        public IEnumerable<EventResult> GetAllEventResultsWithGroup(string runGroupCode, int eventID)
        {
            return _eventResults.GetAll().Where(er => er.RunGroupCode == runGroupCode).Where(er => er.Registration.EventID == eventID).ToList();
        }

        public IEnumerable<EventResult> GetAllEventResultsWithSection(string runSectionCode, int eventID)
        {
            return _eventResults.GetAll().Where(er => er.RunSectionCode == runSectionCode).Where(er => er.Registration.EventID == eventID).ToList();
        }

        /// <summary>
        /// Creates a new EventResultViewModel
        /// </summary>
        /// <param name="eventResults"></param>
        /// <param name="runData"></param>
        /// <returns>EventResultViewModel</returns>
        public EventResultsViewModel CreateEventResultsViewModel(IEnumerable<EventResult> eventResults, IEnumerable<RunData> runData)
        {
            EventResultsViewModel eventResultsViewModel = new EventResultsViewModel
            {
                EventResults = eventResults,
                RunData = runData,
                RunGroup = _runGroup.GetAll(),
                RunSection = _runSection.GetAll()
            };
            return eventResultsViewModel;
        }

        public PartialViewResult GetEventResultsTablePartialView(int eventID)
        {
            IEnumerable<EventResult> eventResultTables = GetAllEventResults(eventID);
            IEnumerable<RunData> eventRunDataTables = _runDatas.GetAllRunDataFromEventResults(eventResultTables);

            EventResultsViewModel eventResultsViewModel = CreateEventResultsViewModel(eventResultTables, eventRunDataTables);

            return PartialView("_EventResultsTable", eventResultsViewModel);
        }

        // GET: Events
        public ActionResult Index()
        {

            return View(_events.GetAll());
        }

        // GET: Events/Details/5
        public ActionResult Details(int? id)
        {

            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Event @event = _events.Get(Convert.ToInt32(id));
            if (@event == null)
            {
                return HttpNotFound();
            }

            else
            {
                var adminEditVm = new EventAdminEditViewModel();
                if (CheckIfCurrentUserIsClubAdmin(@event.EventsID))
                {
                    adminEditVm.ShowAdminControl = true;
                }
                else if (@event.EventDate <= DateTime.Now)
                {
                    adminEditVm.ShowAdminControl = false;
                }
                else
                {
                    adminEditVm.ShowAdminControl = false;
                }
                adminEditVm.Event = @event;
                return View(adminEditVm);
            }

        }

        /// <summary>
        /// Checks to see if a user is a ClubAdmin for a certain event that the club is putting on.
        /// </summary>
        /// <param name="eventID">The event that is being accessed</param>
        /// <returns>Boolean. Is a ClubAdmin or isn't</returns>
        private bool CheckIfCurrentUserIsClubAdmin(int eventID)
        {
            if (User.Identity.IsAuthenticated)
            {
                MotoHubUser user = _motoHubUsers.GetCurrentUser(User.Identity.GetUserId());
                Club club = _clubs.GetClubHoldingEvent(eventID);
                Membership membership = _memberships.GetClubMembership(club.ClubsID, user.MotoHubUserID);

                if (membership != null && membership.MotoHubRole.RoleTitle == "ClubAdmin")
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            else
            {
                return false;
            }
        }

        public bool TestCheckIfCurrentUserIsClubAdmin(int eventID, int clubID, int userID)
        {
            MotoHubUser user = _motoHubUsers.GetAll().Where(u => u.MotoHubUserID == userID).FirstOrDefault();
            Club club = _clubs.GetAll().Where(c => c.ClubsID == clubID).FirstOrDefault();
            Membership membership = _memberships.GetAll().Where(m => m.ClubID == club.ClubsID).Where(m => m.MotoHubUserID == user.MotoHubUserID).FirstOrDefault();

            if (membership != null && membership.RoleID == 1)
            {
                return true;
            }
            else
            {
                return false;
            }

        }

        public bool TestAdmin(int eventID)
        {
            return CheckIfCurrentUserIsClubAdmin(eventID);

        }

        [Authorize]
        // GET: Events/Create
        public ActionResult Create(int? clubID)
        {
            if (clubID == null)
            {
                return RedirectToAction("Index", "Home", null);
            }
            else if (_clubs.IsAdminOfClub(_motoHubUsers.GetCurrentUser(User.Identity.GetUserId()).MotoHubUserID, clubID))
            {
                ViewBag.ClubID = new SelectList(_clubs.GetAll().Where(c => c.ClubsID == clubID), "ClubsID", "ClubName");
                MotoHubUser currentUser = _motoHubUsers.GetCurrentUser(User.Identity.GetUserId());

                ViewData["MotoHubUserID"] = currentUser.MotoHubUserID;

                return View();
            }
            else
            {
                return RedirectToAction("ClubAdminDashboard", "Clubs", new { clubID });
            }
        }

        // POST: Events/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "ClubID,EventsID,EventName,EventDate,EventLocationStreet,EventLocationCity,EventLocationState,EventLocationZip")] Event @event)
        {
            @event.MotoHubUserID = _motoHubUsers.GetCurrentUser(User.Identity.GetUserId()).MotoHubUserID;
            if (ModelState.IsValid)
            {
                @event.GalleryID = CreateInitialGallery();
                CreateInitialPhotoAlbum(@event.GalleryID);

                _events.Add(@event);
                _events.SaveChanges();

                return RedirectToAction("Details", new { id = @event.EventsID });
            }
            ViewBag.ClubID = new SelectList(_clubs.GetAll(), "ClubsID", "ClubName", @event.ClubID);

            return View(@event);
        }

        /// <summary>
        /// Creates a new gallery to be associated with an event.
        /// </summary>
        /// <returns>The id of the new gallery.</returns>
        private int CreateInitialGallery()
        {
            Gallery galleryToAdd = new Gallery();

            _galleries.Add(galleryToAdd);
            _galleries.SaveChanges();

            return galleryToAdd.GalleryID;
        }

        /// <summary>
        /// Creates the initial photo album for the event and gallery passed in.
        /// </summary>
        /// <param name="galleryID">Gallery to associate with the photo album.</param>
        private void CreateInitialPhotoAlbum(int galleryID)
        {
            PhotoAlbum initialEventPhotoAlbum = new PhotoAlbum
            {
                GalleryID = galleryID,
                PhotoAlbumName = "General"
            };

            _photoAlbums.Add(initialEventPhotoAlbum);
            _photoAlbums.SaveChanges();
        }

        [Authorize]
        // GET: Events/Edit/5
        public ActionResult Edit(int? id)
        {

            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Event @event = _events.Get(Convert.ToInt32(id));

            if (@event == null)
            {
                return HttpNotFound();
            }
            if (@event.EventDate <= DateTime.Now)
            {
                return RedirectToAction("EventOver");
            }
            return View(new EventEditViewModel
            {
                EventsID = Convert.ToInt32(id),
                EventName = @event.EventName,
                EventDate = @event.EventDate,
                EventLocationStreet = @event.EventLocationState,
                EventLocationCity = @event.EventLocationCity,
                EventLocationState = @event.EventLocationState,
                EventLocationZip = @event.EventLocationZip
            });
        }

        // POST: Events/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(EventEditViewModel eventEdit)
        {
            if (ModelState.IsValid)
            {
                var dbEvent = _events.Get(eventEdit.EventsID);

                dbEvent.EventName = eventEdit.EventName;
                dbEvent.EventDate = eventEdit.EventDate;
                dbEvent.EventLocationStreet = eventEdit.EventLocationStreet;
                dbEvent.EventLocationCity = eventEdit.EventLocationCity;
                dbEvent.EventLocationState = eventEdit.EventLocationState;
                dbEvent.EventLocationZip = eventEdit.EventLocationZip;

                _events.SetModified(dbEvent);
                _events.SaveChanges();

                return RedirectToAction("Index");
            }
            return View(eventEdit);
        }

        [Authorize]
        // GET: Events/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Event @event = _events.Get(Convert.ToInt32(id));
            if (@event == null)
            {
                return HttpNotFound();
            }
            return View(@event);
        }

        // POST: Events/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Event @event = _events.Get(Convert.ToInt32(id));

            db.Events.Remove(@event);
            db.SaveChanges();

            return RedirectToAction("clubAdminDashboard", "Clubs", new { clubID = @event.ClubID });
        }

        public async System.Threading.Tasks.Task<JavaScriptResult> Venue(int id)
        {
            Event eventobj = _events.Get(Convert.ToInt32(id));
            string street = eventobj.EventLocationStreet;
            string city = eventobj.EventLocationCity;
            string state = eventobj.EventLocationState;
            string zip = eventobj.EventLocationZip.ToString();

            GoogleGeocodingAPI.GoogleAPIKey = System.Web.Configuration.WebConfigurationManager.AppSettings["googlegeocode"];

            Tuple<double, double> result = await GoogleGeocodingAPI.GetCoordinatesFromAddressAsync(street + city + state + zip);

            var script = string.Format(@"function initMap() {{
                    //use the google maps javascript api to make a map object 

                var venue = {{lat: {0}, lng: {1} }};
                console.log(venue.lat, venue.lng);
                var map = new google.maps.Map(document.getElementById('map'), {{
                    zoom: 12,
                    center: venue
                }});
                var marker = new google.maps.Marker({{
                    position: venue,
                    map: map
                }});
            }}", result.Item1, result.Item2);

            return JavaScript(script);
        }

        public ActionResult EventPublicPhotos(int eventID)
        {
            Event eventFound = _events.Get(Convert.ToInt32(eventID));

            EventPhotosViewModel eventPhotosViewModel = new EventPhotosViewModel
            {
                Event = eventFound,
                Photos = db.Photos.Where(p => p.PhotoAlbumID == db.PhotoAlbums
                                  .Where(pa => pa.GalleryID == eventFound.GalleryID)
                                  .Select(pa => pa.PhotoAlbumID)
                                  .FirstOrDefault())
                                  .ToList()
            };

            return View(eventPhotosViewModel);
        }

        private IEnumerable<SelectListItem> UserGarage()
        {
            MotoHubUser currentUser = _motoHubUsers.GetCurrentUser(User.Identity.GetUserId());

            IEnumerable<Garage> userGarage = _garage.GetAll().Where(g => g.MotoHubUserID == currentUser.MotoHubUserID).ToList();

            return new SelectList(userGarage, "Value", "Text");
        }

        [Authorize]
        public ActionResult Registration(int? eventID)
        {
            RegistrationViewModel viewmodel = new RegistrationViewModel();
            ViewBag.Cars = UserGarage();
            return View(viewmodel);
        }

        [Authorize]
        public ActionResult RemoveRegistration(int registrationID)
        {
            Registration registrationToUpdate = _registrations.Get(registrationID);

            _registrations.Remove(registrationToUpdate);
            _registrations.SaveChanges();

            return RedirectToAction("UserHome");
        }

        /// <summary>
        /// Test for PBI 266
        /// </summary>
        /// <param name="registration"></param>
        /// <returns></returns>
        public bool CreatedSuccessfully(Registration registration)
        {
            if (registration.EventID == 0 && registration.UserID == 0)
            {
                throw new Exception("Registration was not created correctly.");
            }
            else
            {
                return true;
            }
        }

        public ActionResult Timing(int id)
        {
            //initially return default view
            int eid = Convert.ToInt32(id);
            IEnumerable<EventResult> results = _eventResults.GetAllEventResultsFromEvent(eid);
            IEnumerable<RunData> runs = _runDatas.GetAllRunDataFromEventResults(results);

            EventResultsViewModel vm = CreateEventResultsViewModel(results, runs);

            return View(vm);
            //ajax call polling database to see if any rundata 
            //replace a div with a partial view to edit the rundata, button for prev
            //ajax call polling database to see next rundata, updates button for next when found
            //call the partial view from ajax
            
        }

        //[HttpPost]
        public ActionResult Queue(int id)
        {
            Event @event = _events.Get(Convert.ToInt32(id));
            return View(@event);
            //qr code scan user code
            //returns registration for the user
            //button for add is activated
            //when pressed, the user is added to the rundata table for this event
        }

        [HttpPost]
        public PartialViewResult GetQueuePartial(QRQueueViewModel q)
        {
            UnitOfWork unitOfWork = new UnitOfWork(db);

            if (q.Type == "U")
            {
                int uid = Convert.ToInt32(q.QrId);
                int eventid = q.Id;
                Registration registration = unitOfWork.Registrations.GetUserRegistrationForEvent(uid, eventid);

                QueueViewModel queuemodel = new QueueViewModel
                {
                    UserID = uid,
                    EventID = eventid,
                    Reg = registration
                };

                return PartialView("_QueuePartial", queuemodel);
            }

            return PartialView("Error");
        }

        [HttpPost]
        public void QueueUser(QueueAddViewModel vm)
        {
            int uid = Convert.ToInt32(vm.UserID);
            int eid = Convert.ToInt32(vm.EventID);
            unitOfWork.RunData.CreateFromUserAndEvent(uid, eid);
        }

        public ActionResult EventOver()
        {
            return View();
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}