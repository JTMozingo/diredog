﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MotoHub.Models;
using MotoHub.Models.ViewModels;
using MotoHub.DAL;
using Microsoft.AspNet.Identity;
using MotoHub.MotoHubDevLibrary;
using QRCoder;
using System.Net;

namespace MotoHub.Controllers

{
    public class VehicleController : Controller
    {

        MotoHubDbContext db = new MotoHubDbContext();
        DevTools devTools = new DevTools();
    
        // GET: Vehicle
        public ActionResult List()
        {
            MotoHubUser currentUser = devTools.GetCurrentUser(db, User.Identity.GetUserId());
            int currentUserID = currentUser.MotoHubUserID;

            var car = db.Cars.Where(m => m.MotoHubUserID == currentUserID).ToList();
            return View(car);
        }


        public ActionResult ListTable()
        {
            MotoHubUser currentUser = devTools.GetCurrentUser(db, User.Identity.GetUserId());
            int currentUserID = currentUser.MotoHubUserID;

            var car = db.Cars.Where(m => m.MotoHubUserID == currentUserID).ToList();
            return View(car);
        }


        [HttpGet]
        [Authorize]
        public ActionResult Create()
        {
            MotoHubUser currentUser = devTools.GetCurrentUser(db, User.Identity.GetUserId());
            int currentUserID = currentUser.MotoHubUserID;

            var clubID = currentUser.Memberships.Select(m => m.ClubID).ToList();
           
            ViewData["Make"] = new SelectList(db.CarMakes.ToList(), "CarMakeID", "Make");
            VehicleViewModel vvm = CreateVehicleViewModel();
            return View(vvm);
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(FormCollection form)
        {
            MotoHubUser currentUser = devTools.GetCurrentUser(db, User.Identity.GetUserId());
            int currentUserID = currentUser.MotoHubUserID;

            Car car = db.Cars.Create();

            car.MotoHubUserID = currentUserID;
            car.GarageID = db.Garages.Where(m => m.MotoHubUserID == currentUserID).Select(m => m.GarageID).FirstOrDefault();

            var carClass = form["CarClass"];
            car.CarClassID = db.CarClasses.Where(m => m.Class == carClass).Select(m => m.CarClassID).FirstOrDefault();
            
            var carMake = form["CarMake"];
            car.CarMakeID = db.CarMakes.Where(m => m.Make == carMake).Select(m => m.CarMakeID).FirstOrDefault();

            car.CarModel = form["Model"];

            var year = form["Year"];
            car.CarYear = Convert.ToInt32(year);

            var carType = form["CarType"];
            car.CarTypeID = db.CarTypes.Where(m => m.CarType1 == carType).Select(m => m.CarTypeID).FirstOrDefault();

            var carColor = form["CarColor"];
            car.CarColorID = db.CarColors.Where(m => m.Color == carColor).Select(m => m.CarColorID).FirstOrDefault();

            if (ModelState.IsValid)
            {
                db.Cars.Add(car);
                db.SaveChanges();

                return RedirectToAction("List");
            }
            return View(Create());
        }


        [HttpGet]
        [Authorize]
        // GET: Events/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Car @car = db.Cars.Find(id);
            if (@car == null)
            {
                return HttpNotFound();
            }
            return View(@car);
        }

        // POST: Events/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult Delete(int id)
        {
            Car @car = db.Cars.Find(id);
           
            db.Cars.Remove(@car);
            db.SaveChanges();

            return RedirectToAction("List");
        }


        [HttpGet]
        [Authorize]
        public ActionResult ListCreator()
        {
            return View();
        }

        [HttpGet]
        public ActionResult ListClasses()
        {
            var car = db.CarClasses.ToList();

            return View(car);
        }

        [HttpGet]
        [Authorize]
        public ActionResult createClass()
        {
           return View();
        }

        [HttpPost]
        [Authorize]
        public ActionResult createClass([Bind(Include = "CarClassID,Class")] CarClass carClass)
        {
            CarClass newClass = db.CarClasses.Create();
            newClass.Class = carClass.Class;

            if (ModelState.IsValid)
            {

                db.CarClasses.Add(newClass);
                db.SaveChanges();
            }

            return RedirectToAction("ListClasses");
        }

        [HttpGet]
        public ActionResult ListMakes()
        {
            var car = db.CarMakes.ToList();

            return View(car);
        }

        [HttpGet]
        [Authorize]
        public ActionResult createMake()
        {
            return View();
        }

        [HttpPost]
        [Authorize]
        public ActionResult createMake([Bind(Include = "CarMakeID,Class")] CarMake carMake)
        {
            if (ModelState.IsValid)
            {
                db.CarMakes.Add(carMake);
                db.SaveChanges();
            }

            return RedirectToAction("ListMakes");
        }



        /*
         * 
         * This is the Dynamic QR code Generation, this returns the Car ID # found in the database.
         * The value should be static as there is no way to update the car# but the call will happen new each time just as a safety measure
         * */

        public ActionResult GenerateQRCode(int CarParam)
        {
            //get the current user and retrieve their MotoHub ID
            MotoHubUser currentUser = devTools.GetCurrentUser(db, User.Identity.GetUserId());
            int currentUserID = currentUser.MotoHubUserID;

            QRCodeGenerator qrGenerator = new QRCodeGenerator();

            //start the parameter with a C, to identify later when the QRCode is scanned
            string qrString = "C" + CarParam.ToString();

            //convert the ID to a string and pass it to the QRCode generator
            QRCodeData qrCodeData = qrGenerator.CreateQrCode(qrString, QRCodeGenerator.ECCLevel.Q);
            PngByteQRCode qrCode = new PngByteQRCode(qrCodeData);
            //Bitmap qrCodeImage = 
            byte[] qrCodePngByte = qrCode.GetGraphic(10);

            Car car = db.Cars.Where(m => m.CarID == CarParam).FirstOrDefault();

            db.SaveChanges();
            return File(qrCodePngByte, "image/png");
        }


        [Authorize]
        private VehicleViewModel CreateVehicleViewModel()
        {
            int userID = devTools.GetCurrentUser(db, User.Identity.GetUserId()).MotoHubUserID;

            VehicleViewModel vehicle = new VehicleViewModel
            {
                Garage = db.Garages.Where(m => m.MotoHubUserID == userID).ToList(),

                Car = db.Cars.Where(m => m.MotoHubUserID == userID).ToList(),

                CarClass = db.CarClasses.ToList(),

                CarColor = db.CarColors.ToList(),

                CarMake = db.CarMakes.ToList(),

                CarType = db.CarTypes.ToList(),

                };

            return vehicle;
        }

    }
}