﻿using Microsoft.AspNet.Identity;
using MotoHub.DAL;
using MotoHub.Models;
using MotoHub.Models.ViewModels;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Data.Entity;
using System.Web;
using System.Web.Mvc;
using QRCoder;
using MotoHub.MotoHubDevLibrary;
using MotoHub.Repositories.Persistance;
using MotoHub.Repositories.RepositoryInterfaces;

namespace MotoHub.Controllers
{
    public class ProfileController : Controller
    {
        MotoHubDbContext db = new MotoHubDbContext();

        DevTools devTools = new DevTools();
        UnitOfWork unitOfWork = new UnitOfWork(new MotoHubDbContext());

        private IEventRepository _events;
        private IClubRepository _clubs;
        private IMembershipRepository _memberships;
        private IRoleRepository _roles;
        private IMotoHubUserRepository _motoHubUsers;
        private IRunDataRepository _runDatas;
        private IEventResultsRepository _eventResults;
        private IRegistrationRepository _registrations;
        private IGalleriesRepository _galleries;
        private IPhotoAlbumsRepository _photoAlbums;
        private IPhotosRepository _photos;
        private IGarageRepository _garage;
        private IRunGroupRepository _runGroup;
        private IRunSectionRepository _runSection;
        private IPendingClubInvitationsRepository _pendingInvites;

        public ProfileController() { }

        public ProfileController(IPendingClubInvitationsRepository invitesRepo, IEventRepository eventRepository, IClubRepository clubRepo, IMembershipRepository memRepo, IRoleRepository roleRepository, IMotoHubUserRepository userRepo, IRunDataRepository runDataRepo, IEventResultsRepository eventResultsRepo, IRegistrationRepository registrationRepo, IGalleriesRepository galleriesRepo, IPhotoAlbumsRepository photoAlbumsRepo, IPhotosRepository photosRepo, IGarageRepository garageRepo, IRunGroupRepository runGroupRepo, IRunSectionRepository runSectionRepo)
        {
            _pendingInvites = invitesRepo;
            _events = eventRepository;
            _clubs = clubRepo;
            _memberships = memRepo;
            _roles = roleRepository;
            _motoHubUsers = userRepo;
            _runDatas = runDataRepo;
            _eventResults = eventResultsRepo;
            _registrations = registrationRepo;
            _galleries = galleriesRepo;
            _photoAlbums = photoAlbumsRepo;
            _photos = photosRepo;
            _garage = garageRepo;
            _runGroup = runGroupRepo;
            _runSection = runSectionRepo;
        }
        /// <summary>
        /// This will update the the LoginCount for this specific user.
        /// </summary>
        /// <returns>
        /// A redirect to the Users Home View
        /// </returns>
        [Authorize]
        public ActionResult ManageSuccessfulUserLogin()
        {
            MotoHubUser userToUpdate = unitOfWork.MotoHubUsers.GetCurrentUser(User.Identity.GetUserId());
            unitOfWork.MotoHubUsers.IncrementLoginCount(userToUpdate.MotoHubUserID);
            unitOfWork.Complete();
            /*
        db.Entry(userToUpdate).State = EntityState.Modified;

        db.SaveChanges();
        */
            return RedirectToAction("UserHome");
        }

        /// <summary>
        /// Gets the home view page of the logged in user.
        /// </summary>
        /// <returns>
        /// returns the users home view.
        /// </returns>
        [Authorize]
        public ActionResult UserHome()
        {
            UserProfileViewModel userProfileViewModel = CreateUserProfileViewModel();

            return View(userProfileViewModel);
        }

        [Authorize]
        private UserProfileViewModel CreateUserProfileViewModel()
        {
            int userID = devTools.GetCurrentUser(db, User.Identity.GetUserId()).MotoHubUserID;

            UserProfileViewModel userProfileViewModel = new UserProfileViewModel
            {
                MotoHubUsers = db.MotoHubUsers.Where(m => m.MotoHubUserID == userID),

                UserProfile = db.MotoHubUsers.Where(up => up.MotoHubUserID == userID)
                                             .Select(mu => mu.UserProfile),

                Memberships = db.Memberships.Where(m => m.MotoHubUser.MotoHubUserID == userID)
                                            .ToList(),

                Car = db.Cars.Where(m => m.MotoHubUserID == userID).ToList(),

                CarClass = db.CarClasses.ToList(),

                CarColor = db.CarColors.ToList(),

                CarMake = db.CarMakes.ToList(),

                Cartype = db.CarTypes.ToList(),

                Registration = db.Registrations.Where(r => r.UserID == userID)
                                               .ToList(),

                Event = db.Events.ToList(),

                PendingClubInvitation = db.PendingClubInvitations.Where(pci => pci.MotoHubUserID == userID).Where(pci => pci.Rejected == false).ToList()
            };

            return userProfileViewModel;
        }

        public PartialViewResult GetMembershipInvitationPartial()
        {
            IEnumerable<PendingClubInvitation> pendingClubInvitation = _pendingInvites.GetAll().Where(pci => pci.MotoHubUserID == _motoHubUsers.GetCurrentUser(User.Identity.GetUserId()).MotoHubUserID).Where(pci => pci.Rejected == false).ToList();
            return PartialView("_PendingClubInvitationsPartialView", pendingClubInvitation);
        }

        /// <summary>
        /// Call to edit the authorized user's account information.
        /// </summary>
        /// <returns>
        /// The edit view for the current user.
        /// </returns>
        [Authorize]
        public ActionResult UserEditAccountInformation()
        {
            MotoHubUser currentUser = devTools.GetCurrentUser(db, User.Identity.GetUserId());
            UserProfile userProfile = devTools.GetCurrentUserProfileModel(db, currentUser);

            return View(userProfile);
        }

        /// <summary>
        /// Generates a PNG QRCode that stores the users ID as a string
        /// </summary>
        /// <returns>A byte array as a PNG image file.</returns>
        public ActionResult GenerateQRCode()
        {
            //get the current user and retrieve their MotoHub ID
            MotoHubUser currentUser = devTools.GetCurrentUser(db, User.Identity.GetUserId());
            int currentUserID = currentUser.MotoHubUserID;

            QRCodeGenerator qrGenerator = new QRCodeGenerator();

            string qrString = "U" + currentUserID.ToString();

            //convert the ID to a string and pass it to the QRCode generator
            QRCodeData qrCodeData = qrGenerator.CreateQrCode(qrString, QRCodeGenerator.ECCLevel.Q);
            PngByteQRCode qrCode = new PngByteQRCode(qrCodeData);

            byte[] qrCodePngByte = qrCode.GetGraphic(10);
            return File(qrCodePngByte, "image/png");
        }
        /// <summary>
        /// Processes user input to edit UserProfile personal information. The only information that will be possible to
        /// edit here will be the UserProfile rows: FirstName, LastName, DateOfBirth and Bio.
        /// </summary>
        /// <param name="userToUpdate">UserProfile database entity containing the information that the user wants to
        /// update.
        /// </param>
        /// <returns>Redirect to the UserHome view.</returns>
        [HttpPost]
        public ActionResult UserEditAccountInformation([Bind(Include = "FirstName, LastName, DateOfBirth, Bio")] UserProfile userToUpdate)
        {
            if (ModelState.IsValid)
            {
                MotoHubUser hubUser = devTools.GetCurrentUser(db, User.Identity.GetUserId());
                UserProfile userProfile = devTools.GetCurrentUserProfileModel(db, hubUser);

                userProfile.FirstName = userToUpdate.FirstName;
                userProfile.LastName = userToUpdate.LastName;
                userProfile.DateOfBirth = userToUpdate.DateOfBirth;
                userProfile.Bio = userToUpdate.Bio;

                db.SaveChanges();

                return RedirectToAction("UserHome");
            }
            else
            {
                MotoHubUser currentUser = devTools.GetCurrentUser(db, User.Identity.GetUserId());
                UserProfile userProfile = devTools.GetCurrentUserProfileModel(db, currentUser);

                return View(userProfile);
            }

        }

        /// <summary>
        /// Create and add the new membership relation between the club and the member.
        /// </summary>
        /// <param name="clubID">Club that the user is trying to join.</param>
        /// <returns>Redirects the user back to their homepage.</returns>
        [Authorize]
        public ActionResult AddUserToClub(int? clubID)
        {
            if (clubID != null)
            {
                AddUserToClubWithoutRedirect((int)clubID);
            }

            return RedirectToAction("UserHome");
        }

        /// <summary>
        /// Helper method to avoid being required to redirect to somewhere when adding a user to a club.
        /// </summary>
        /// <param name="clubID">ClubID of Club to add the user to.</param>
        [HttpPost]
        public void AddUserToClubWithoutRedirect(int clubID)
        {
            MotoHubUser currentUser = _motoHubUsers.GetCurrentUser(User.Identity.GetUserId());
            Club clubToJoin = _clubs.Get(clubID);
            PendingClubInvitation pendingClubInvitation = _pendingInvites.GetPendingClubInvitation(currentUser.MotoHubUserID, clubToJoin.ClubsID);

            if (pendingClubInvitation != null)
            {
                _pendingInvites.Remove(pendingClubInvitation);
                _pendingInvites.SaveChanges();
            }

            Membership membership = new Membership
            {
                ClubID = clubToJoin.ClubsID,
                MotoHubUserID = currentUser.MotoHubUserID,
                Status = true,
                RoleID = _roles.GetRole("ClubMember").RoleID,
                DuesPaid = false
            };

            _memberships.Add(membership);
            _memberships.SaveChanges();
        }

        [HttpPost]
        public void RejectMembershipInvitation(int clubID)
        {
            PendingClubInvitation invitationToUpdate = _pendingInvites.GetPendingClubInvitation(_motoHubUsers.GetCurrentUser(User.Identity.GetUserId()).MotoHubUserID, clubID);
            invitationToUpdate.Rejected = true;

            _pendingInvites.SetModified(invitationToUpdate);
            _pendingInvites.SaveChanges();
        }

        public ActionResult UserEventResults()
        {
            return View();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public ActionResult UserEvents()
        {
            int userID = _motoHubUsers.GetCurrentUser(User.Identity.GetUserId()).MotoHubUserID;

            return View(GetAllUserEvents(userID));
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="userID"></param>
        /// <returns></returns>
        private IEnumerable<Event> GetAllUserEvents(int userID)
        {
            IEnumerable<Registration> allUserRegistrations = _registrations.GetAllUserRegistrations(userID);
            IEnumerable<Event> allEventsFromUserRegistrations = _events.GetAllEventsFromListOfRegistrations(allUserRegistrations);

            return allEventsFromUserRegistrations;
        }

        /// <summary>
        /// 
        /// </summary>
        [HttpPost]
        public PartialViewResult GetUserPastEvents()
        {
            int userID = _motoHubUsers.GetCurrentUser(User.Identity.GetUserId()).MotoHubUserID;

            IEnumerable<Event> eventsToFilter = GetAllUserEvents(userID);
            List<Event> pastEvents = new List<Event>();

            foreach (var @event in eventsToFilter)
            {
                if (@event.EventDate < System.DateTime.Now)
                {
                    pastEvents.Add(@event);
                }
            }

            return PartialView("_UserEventsTablePartialView", pastEvents);
        }

        [HttpPost]
        public PartialViewResult GetUserUpcomingEvents()
        {
            int userID = _motoHubUsers.GetCurrentUser(User.Identity.GetUserId()).MotoHubUserID;

            IEnumerable<Event> eventsToFilter = GetAllUserEvents(userID);
            List<Event> upcomingEvents = new List<Event>();

            foreach (var @event in eventsToFilter)
            {
                if (@event.EventDate >= System.DateTime.Now)
                {
                    upcomingEvents.Add(@event);
                }
            }

            return PartialView("_UserEventsTablePartialView", upcomingEvents);
        }

        public PartialViewResult GetUserEventsTablePartialView(IEnumerable<Event> eventList)
        {
            return PartialView("_UserEventsTablePartialView", eventList);
        }

        public ActionResult LeaveCLub(int clubID)
        {
            db.Memberships.Remove(db.Memberships.Where(m => m.ClubID == clubID).FirstOrDefault());
            db.SaveChanges();

            return RedirectToAction("UserHome");
        }

        /// <summary>
        /// This will return a view of all UserProfiles. This includes checking to see if the
        /// user is authenticated or not. If they are not then all users are displayed, but if
        /// they are then the user who is searching's profile is removed.
        /// </summary>
        /// <returns>View of all (or most) UserProfiles in MotoHub.</returns>
        [AllowAnonymous]
        public ActionResult UsersSearch()
        {
            if (User.Identity.IsAuthenticated)
            {
                int currentUserID = devTools.GetCurrentUser(db, User.Identity.GetUserId()).MotoHubUserID;
                return View(db.UserProfiles.Where(u => u.MotoHubUserID != currentUserID).ToList());
            }
            else
            {
                return View(db.UserProfiles.ToList());
            }
        }

        /// <summary>
        /// Process a request to view a users profile. This checks if the user's profile is currently
        /// listed as public or private and passes model information accordingly.
        /// </summary>
        /// <param name="userID">ID of profile owner.</param>
        /// <returns>Users public profile view.</returns>
        public ActionResult UserPublicProfile(int userID)
        {
            UserProfile userProfile = db.UserProfiles
                                        .Where(u => u.MotoHubUserID == userID)
                                        .FirstOrDefault();

            /////Test
            ///userProfile.ProfileIsPrivate = true;

            if (userProfile.ProfileIsPrivate == true)
            {
                userProfile = null;
                return View(userProfile);
            }
            else
            {
                return View(userProfile);
            }
        }

        public FileContentResult GetProfilePhoto()
        {
            MotoHubUser hubUser = devTools.GetCurrentUser(db, User.Identity.GetUserId());
            UserProfile userProfile = devTools.GetCurrentUserProfileModel(db, hubUser);

            Photo profilePhoto = db.Photos.Where(p => p.PhotoID == userProfile.ProfilePhotoID).FirstOrDefault();

            return File(profilePhoto.PhotoData, profilePhoto.ContentType);

        }

        public ActionResult UserPublicPhotos(int galleryID)
        {
            UserProfileViewModel userPhotos = new UserProfileViewModel
            {
                Photos = db.Photos.Where(p => p.PhotoAlbum.GalleryID == galleryID).ToList()
            };

            return View(userPhotos);
        }

        /// <summary>
        /// Test for PBI 266
        /// </summary>
        /// <param name="registration"></param>
        /// <returns></returns>
        public bool CreatedSuccessfully(Registration registration)
        {
            if (registration.EventID == 0 && registration.UserID == 0)
            {
                throw new Exception("Registration was not created correctly.");
            }
            else
            {
                return true;
            }
        }

        public ViewResult UserProfileResultsChart()
        {
            return View();
        }
    }
}