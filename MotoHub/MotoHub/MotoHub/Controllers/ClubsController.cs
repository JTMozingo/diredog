﻿using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using MotoHub.DAL;
using MotoHub.Models;
using MotoHub.Models.ViewModels;
using MotoHub.MotoHubDevLibrary;
using MotoHub.Repositories.RepositoryInterfaces;

namespace MotoHub.Controllers
{
    public class ClubsController : Controller
    {
        private IMotoHubDbContext db = new MotoHubDbContext();
        private IMotoHubUserRepository _motoHubUsers;
        private IClubRepository _clubs;
        private IMembershipRepository _memberships;
        private IRoleRepository _roles;
        private IPendingClubInvitationsRepository _pendingClubInvites;

        private DevTools devTools = new DevTools();

        public ClubsController() { }

        public ClubsController(IMotoHubDbContext context)
        {
            db = context;
        }

        public ClubsController(IMotoHubUserRepository motoUserRepo, IClubRepository clubRepo, IPendingClubInvitationsRepository pendingClubInvitesRepo)
        {
            _motoHubUsers = motoUserRepo;
            _clubs = clubRepo;
            _pendingClubInvites = pendingClubInvitesRepo;
        }

        // GET: Clubs

        public ActionResult Index()
        {
            if (User.Identity.IsAuthenticated)
            {
                MotoHubUser currentUser = devTools.GetCurrentUser(db, User.Identity.GetUserId());
                UserProfileViewModel model = new UserProfileViewModel
                {
                    MotoHubUsers = db.MotoHubUsers.Where(m => m.MotoHubUserID == currentUser.MotoHubUserID),
                    Memberships = db.Memberships.Where(m => m.MotoHubUserID == currentUser.MotoHubUserID).ToList(),
                    Clubs = db.Clubs.ToList()
                };

                return View(model);
            }
            else
            {
                UserProfileViewModel model = new UserProfileViewModel
                {
                    Clubs = db.Clubs.ToList()
                };

                return View(model);
            }
        }

        /// <summary>
        /// Get the view of the club public profile.
        /// </summary>
        /// <param name="clubID">ClubsID of club to be shown.</param>
        /// <returns>View of club public profile.</returns>
        [AllowAnonymous]
        public ActionResult ClubPublicProfile(int clubID)
        {
            Club clubFound = db.Clubs.Find(clubID);

            ClubPhotosViewModel viewModel = CreateClubPhotosViewModel(clubFound, clubFound.GalleryID);

            return View(viewModel);
        }


        /// <summary>
        /// Creates a new ClubPhotosViewModel based on the passed clubID
        /// </summary>
        /// <param name="clubID">Club to insert into the ViewModel</param>
        /// <param name="galleryID">Gallery associated with that club.</param>
        /// <returns></returns>
        public ClubPhotosViewModel CreateClubPhotosViewModel(Club clubFound, int? galleryID)
        {
            if (clubFound == null)
            {
                throw new Exception("Input club was null.");
            }
            else if (galleryID == null || galleryID <= 0)
            {
                throw new Exception("Input galleryID was null or 0.");
            }
            else
            {
                ClubPhotosViewModel clubPhotosViewModel = new ClubPhotosViewModel
                {
                    Clubs = clubFound,
                    Photos = db.Photos.Where(p => p.PhotoAlbum.GalleryID == galleryID).ToList()
                };

                return clubPhotosViewModel;
            }

        }

        /// <summary>
        /// Get the view of the club admin dashboard.
        /// </summary>
        /// <param name="clubID">ClubsID of the club to get.</param>
        /// <returns>View of Admin Dashboard.</returns>
        [Authorize]
        public ActionResult ClubAdminDashboard(int? clubID)
        {
            if (clubID == null)
            {
                return RedirectToAction("Index", "Home", null);
            }
            else
            {
                int currentUserID = _motoHubUsers.GetCurrentUser(User.Identity.GetUserId()).MotoHubUserID;

                if (_clubs.IsAdminOfClub(currentUserID, clubID))
                {
                    return View(db.Clubs.Find(clubID));
                }
                else
                {
                    return RedirectToAction("Index", "Home", null);
                }
            }
        }

        /// <summary>
        /// Will call the profile controller to handle the authentication of the user and
        /// add that user to the club.
        /// </summary>
        /// <param name="id">id of the club the user wants to join.</param>
        /// <returns>redirects to the profile controller.</returns>
        public ActionResult Join(int? id)
        {
            Club clubToUpdate = db.Clubs.Find(id);

            clubToUpdate.ClubSize = clubToUpdate.Memberships.Count + 1;

            //change for dependency injection
            //db.Entry(clubToUpdate).State = EntityState.Modified;
            db.MarkAsModified(clubToUpdate);

            db.SaveChanges();

            return RedirectToAction("AddUserToClub", "Profile", new { clubID = id });
        }

        // GET: Clubs/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Club club = db.Clubs.Find(id);
            if (club == null)
            {
                return HttpNotFound();
            }
            return View(club);
        }

        // GET: Clubs/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Clubs/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "ClubsID,ClubName,ClubDescription,ClubLocationStreet,ClubLocationCity,ClubLocationState,ClubLocationZip")] Club club)
        {
            if (ModelState.IsValid && !db.Clubs.Any(c => c.ClubName == club.ClubName))
            {
                int galleryID = CreateInitialClubGallery();
                club.GalleryID = galleryID;

                db.Clubs.Add(club);
                db.SaveChanges();

                CreateInitialPhotoAlbum(galleryID);
                AssignInitialAdminMembership(club.ClubsID);

                return RedirectToAction("ClubAdminDashboard", new { clubID = club.ClubsID });
            }
            else
            {
                ModelState.AddModelError("", "Sorry, looks like this club name is already taken. Please choose another one.");
            }
            return View(club);
        }

        /// <summary>
        /// Creates the first album that a club will have. This album is just titled General as a default.
        /// </summary>
        /// <param name="galleryID">The gallery that the photo album will live in.</param>
        private void CreateInitialPhotoAlbum(int galleryID)
        {
            PhotoAlbum newPhotoAlbum = new PhotoAlbum
            {
                GalleryID = galleryID,
                PhotoAlbumName = "General"
            };

            db.PhotoAlbums.Add(newPhotoAlbum);
            db.SaveChanges();

        }

        /// <summary>
        /// Creates a gallery for the club to use.
        /// </summary>
        /// <returns>The ID of the created gallery.</returns>
        private int CreateInitialClubGallery()
        {
            Gallery galleryToAdd = new Gallery();

            db.Galleries.Add(galleryToAdd);
            db.SaveChanges();

            return galleryToAdd.GalleryID;
        }

        /// <summary>
        /// This will be used to assign admin and membership relationship to a club when it is
        /// first created. The method will initiate the currently logged in user to become the admin.
        /// </summary>
        /// <param name="clubID"></param>
        private void AssignInitialAdminMembership(int clubID)
        {
            MotoHubUser currentUser = devTools.GetCurrentUser(db, User.Identity.GetUserId());

            Membership newMembership = new Membership
            {
                MotoHubUserID = db.MotoHubUsers.Find(currentUser.MotoHubUserID).MotoHubUserID,
                ClubID = clubID,
                RoleID = db.MotoHubRoles
                           .Where(r => r.RoleTitle == "ClubAdmin")
                           .Select(r => r.RoleID)
                           .FirstOrDefault(),
                Status = true,
                DuesPaid = false
            };

            db.Memberships.Add(newMembership);
            db.SaveChanges();
        }

        // GET: Clubs/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Club club = db.Clubs.Find(id);
            if (club == null)
            {
                return HttpNotFound();
            }
            return View(club);
        }

        /// <summary>
        /// Post action resulting in an update to the database of any of the club's information.
        /// </summary>
        /// <param name="club">club object created from user modifications.</param>
        /// <returns>Redirects the View to the Admin Dashboard.</returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "ClubsID,ClubName,ClubDescription,ClubLocationStreet,ClubLocationCity,ClubLocationState,ClubLocationZip,ClubIsActive")] Club club)
        {
            if (ModelState.IsValid)
            {
                //db.Entry(club).State = EntityState.Modified;
                db.MarkAsModified(club);
                db.SaveChanges();
                return RedirectToAction("ClubAdminDashboard", new { clubID = club.ClubsID });
            }
            return RedirectToAction("ClubAdminDashboard", club.ClubsID);
        }

        /// <summary>
        /// Post action resulting in an update to the database of the club's name.
        /// </summary>
        /// <param name="clubID">the Id of the club to edit.</param>
        /// <param name="newClubName">The new name for the club.</param>
        /// <returns>Returns the View control to the Admin Dashboard.</returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult EditClubName(int clubID, string newClubName)
        {
            if (newClubName.Length == 0)
            {
                return RedirectToAction("ClubAdminDashboard", new { clubID });
            }
            else
            {
                Club clubToEdit = db.Clubs.Find(clubID);

                clubToEdit.ClubName = newClubName;

                db.MarkAsModified(clubToEdit);
                db.SaveChanges();

                return RedirectToAction("ClubAdminDashboard", new { clubID });
            }
        }

        /// <summary>
        /// The delete has been confirmed by the user and will now excecute and act on the database.
        /// </summary>
        /// <param name="clubID">Id of club to be deleted</param>
        /// <returns>View of the user's profile.</returns>
        [Authorize]
        public ActionResult DeleteConfirmed(int clubID)
        {
            Club club = db.Clubs.Find(clubID);

            db.Clubs.Remove(club);
            db.SaveChanges();

            return RedirectToAction("UserHome", "Profile");
        }


        /// <summary>
        /// This method is used to return the partial view of a membership delete confirmation modal.
        /// The partial view is used to avoid over population of modals in the ClubAdmin Dashboard.
        /// </summary>
        /// <param name="clubMembership">Membership to be checked.</param>
        /// <returns>PartialView of confirmation modal.</returns>
        [HttpGet]
        public ActionResult ConfirmDeleteClubMember(Membership clubMembership)
        {
            Membership memberToConfirm = db.Memberships.Where(m => m.MotoHubUserID == clubMembership.MotoHubUserID && m.ClubID == clubMembership.ClubID).FirstOrDefault();

            return PartialView("_ConfirmDeleteClubMember", memberToConfirm);
        }

        /// <summary>
        /// Membership removal function. Removes club member from the club by deleting the link in the Membership table.
        /// </summary>
        /// <param name="clubMembership">Membership to delete.</param>
        /// <returns>View of the updated ClubAdminDashboard.</returns>
        [Authorize]
        public ActionResult DeleteClubMember(Membership clubMembership)
        {
            Membership membershipToRemove = db.Memberships.Where(m => m.ClubID == clubMembership.ClubID && m.MotoHubUserID == clubMembership.MotoHubUserID).FirstOrDefault();
            db.Memberships.Remove(membershipToRemove);
            db.SaveChanges();

            return RedirectToAction("ClubAdminDashboard", new { clubID = clubMembership.ClubID });
        }

        public bool OnlyOneClubMember(int clubMembersCount)
        {
            if (clubMembersCount < 1)
            {
                throw new Exception("Clubs should not have 0 or fewer members.");
            }

            return clubMembersCount == 1;
        }
        ///helper method that checks if the member is the last admin member of the club
        ///TODO: Change this to take Membership as a parameter after the Context is changed to an interface


        /// <summary>
        /// Creates a class to the EventsController's Delete action.
        /// </summary>
        /// <param name="eventID"></param>
        /// <returns>Redirection to the EventsController Delete View</returns>
        public ActionResult DeleteEvent(int eventID)
        {
            return RedirectToAction("Delete", "Events", new { id = eventID });
        }

        /// <summary>
        /// This will retrieve a partial view of a modal containing all of the current members in the club.
        /// </summary>
        /// <param name="clubID">club to get members from.</param>
        /// <returns>A partialView of a modal to the current view.</returns>
        [HttpGet]
        public ActionResult GetPartialViewAddMembership(int clubID)
        {

            Club clubTest = db.Clubs.Find(clubID);

            IEnumerable<MotoHubUser> usersInClub = clubTest.Memberships.Select(m => m.MotoHubUser);
            IEnumerable<MotoHubUser> usersAlreadyInvited = db.PendingClubInvitations.Where(pci => pci.ClubID == clubID).Select(pci => pci.UserProfile.MotoHubUser); 
            IEnumerable<MotoHubUser> allUsers = db.MotoHubUsers;
            IEnumerable<MotoHubUser> userTest = allUsers.Except(usersInClub).Except(usersAlreadyInvited).ToList();

            ViewBag.ClubID = clubID;
            return PartialView("_AddClubMembership", userTest);
        }

        /// <summary>
        /// Creates a new membership based on an array of userIDs associated with the clubID that is passed in.
        /// </summary>
        /// <param name="usersToAdd">array of userIDs to add to the membership.</param>
        /// <param name="clubID">Club assocaited with the membership.</param>
        /// <returns></returns>
        [Authorize]
        [HttpPost]
        public ActionResult AddMembership(int[] usersToAdd, int clubID)
        {
            for (var i = 0; i < usersToAdd.Length; i++)
            {
                int userID = usersToAdd[i];
                MotoHubUser user = db.MotoHubUsers.Find(userID);
                if (user != null)
                {
                    Membership newMembership = new Membership
                    {
                        MotoHubUserID = user.MotoHubUserID,
                        ClubID = clubID,
                        RoleID = 2,
                        Status = true,
                        DuesPaid = false
                    };

                    db.Memberships.Add(newMembership);
                    db.SaveChanges();
                }
                else
                {
                    throw new Exception("This member could not be located in the database");
                }

            }

            return RedirectToAction("ClubAdminDashboard", new { clubID });
        }

        /// <summary>
        /// Updates the PendingClubInvitations table to hold a clubID and a userID. 
        /// </summary>
        /// <param name="userIDs">List of users to send invites to.</param>
        /// <param name="clubID">ClubID that will be sending the invite.</param>
        /// <returns></returns>
        [Authorize]
        [HttpPost]
        public ActionResult SendMembershipInvitation(int[] userIDs, int clubID)
        {
            for (var i = 0; i < userIDs.Length; i++)
            {
                int userID = userIDs[i];
                PendingClubInvitation newInvitation = new PendingClubInvitation
                {
                    ClubID = clubID,
                    MotoHubUserID = userID,
                    Rejected = false
                };

                _pendingClubInvites.Add(newInvitation);
                _pendingClubInvites.SaveChanges();
                
            }

            return RedirectToAction("ClubAdminDashboard", new { clubID });
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        public FileContentResult GetProfilePhoto(int clubID)
        {
            Club club = db.Clubs.Find(clubID);
            Photo profilePhoto = db.Photos.Where(p => p.PhotoID == db.Clubs.Where(c => c.ClubsID == clubID).Select(c => c.ProfilePhotoID).FirstOrDefault()).FirstOrDefault();

            return File(profilePhoto.PhotoData, profilePhoto.ContentType);

        }

    }
}
