﻿using Microsoft.AspNet.Identity;
using MotoHub.Models;
using MotoHub.Models.ViewModels;
using MotoHub.Repositories.RepositoryInterfaces;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Helpers;
using System.Web.Mvc;
using System.Web.Script.Services;
using System.Web.Services;

namespace MotoHub.Controllers
{
    public class ChartController : Controller
    {
        private IEventResultsRepository _eventResultsRepo;
        private IMotoHubUserRepository _motoHubUserRepo;
        private IRunDataRepository _runDataRepo;
        private IEventRepository _eventsRepo;
        private DateTime baseDate;

        public ChartController()
        {

        }

        public ChartController(IEventResultsRepository eventResultsRepo, IMotoHubUserRepository motoHubUserRepo, IRunDataRepository runDataRepo, IEventRepository eventRepo)
        {
            _eventResultsRepo = eventResultsRepo;
            _runDataRepo = runDataRepo;
            _motoHubUserRepo = motoHubUserRepo;
            _eventsRepo = eventRepo;
        }

        public ChartController(IEventRepository eventRepo)
        {
            _eventsRepo = eventRepo;
        }

        public ChartController(IRunDataRepository runDataRepo)
        {
            _runDataRepo = runDataRepo;
        }

        // GET: Chart
        public ActionResult Index()
        {

            return View();
        }

        /// <summary>
        /// Returns a JSON object containing the best (lowest) Adjusted Run Times for the current user at all
        /// of their past events.
        /// </summary>
        /// <returns>JSON object</returns>
        public ActionResult GetAdjustedRunTimeProgressionOverTimeChart()
        {
            MotoHubUser currentUser = _motoHubUserRepo.GetCurrentUser(User.Identity.GetUserId());

            IEnumerable<EventResult> userEventResults = _eventResultsRepo.GetAllPastEventResultsFromCurrentUser(currentUser, _eventResultsRepo);
            if (userEventResults.Count() == 0)
            {
                return Json(new { eventDate = new DateTime(System.DateTime.Now.Year, 1, 1), convertedTimes = ConvertDotNetDateTimeToJavaScriptDate(ConvertTimeSpanToDateTime(new TimeSpan())) }, JsonRequestBehavior.AllowGet);
            }
            IEnumerable<RunData> userRunData = _runDataRepo.GetAllRunDataFromEventResults(userEventResults);
            IEnumerable<RunData> lowestAdjustedTimeRunData = _runDataRepo.GetBestAdjustedTimeFromRunData(userRunData, userEventResults);
            IEnumerable<RunData> orderedLowestAdjustedTimeRunData = lowestAdjustedTimeRunData.OrderBy(rd => rd.EventResult.Registration.Event.EventDate);

            return Json(orderedLowestAdjustedTimeRunData.Select(d => new
            {
                eventDate = ConvertDotNetDateTimeToJavaScriptDate(d.EventResult.Registration.Event.EventDate),
                convertedTimes = ConvertDotNetDateTimeToJavaScriptDate(ConvertTimeSpanToDateTime(d.AdjustedTime))
            }),
                                                                    JsonRequestBehavior.AllowGet);
        }


        public ActionResult GetAverageAdjustedTimeChart()
        {
            MotoHubUser currentUser = _motoHubUserRepo.GetCurrentUser(User.Identity.GetUserId());

            IEnumerable<EventResult> userEventResults = _eventResultsRepo.GetAllPastEventResultsFromCurrentUser(currentUser, _eventResultsRepo);

            if (userEventResults.Count() <= 0)
            {
                return Json(new { eventDate = new DateTime(System.DateTime.Now.Year, 1, 1), convertedTimes = ConvertDotNetDateTimeToJavaScriptDate(ConvertTimeSpanToDateTime(new TimeSpan())) }, JsonRequestBehavior.AllowGet);
            }
            else
            {

                IEnumerable<RunData> userRunData = _runDataRepo.GetAllRunDataFromEventResults(userEventResults);
                IEnumerable<EventResultAverageTime> averageAdjustedTimes = _runDataRepo.GetAverageAdjustedTimeFromRunData(userRunData, userEventResults);

                return Json(averageAdjustedTimes.Select(aat => new
                {
                    eventDate = ConvertDotNetDateTimeToJavaScriptDate(aat.EventDate),
                    convertedTimes = ConvertDotNetDateTimeToJavaScriptDate(ConvertTimeSpanToDateTime(aat.AverageTime))
                }), JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult GetAveragePointsForTheYear()
        {
            MotoHubUser currentUser = _motoHubUserRepo.GetCurrentUser(User.Identity.GetUserId());

            IEnumerable<EventResult> userEventResults = _eventResultsRepo.GetAllPastEventResultsFromCurrentUser(currentUser, _eventResultsRepo);
            IEnumerable<RunData> userRunData = _runDataRepo.GetAllRunDataFromEventResults(userEventResults);
            IEnumerable<int> listOfEventYears = _eventResultsRepo.GetAllYearsForEventsParticipatedIn(userEventResults);
            List<double> listOfResultAverages = new List<double>();

            if (listOfEventYears.Count() == 0)
            {
                var chartData = new object[2];
                chartData[0] = new object[]
                {
                "Year",
                "Average Points Earned",

                };
                chartData[1] = new object[]
                {
                    ConvertDotNetDateTimeToJavaScriptDate(new DateTime(System.DateTime.Now.Year, 1, 1)),
                    0
                };

                return Json(chartData, JsonRequestBehavior.AllowGet);
            }
            else
            {
                foreach (var year in listOfEventYears)
                {
                    listOfResultAverages.Add((double)_eventResultsRepo.GetAveragePointsEarned(_eventResultsRepo.GetAllEventResultsFromYear(userEventResults, year)));
                }

                var chartData = new object[listOfResultAverages.Count() + 1];
                chartData[0] = new object[]
                {
                "Year",
                "Average Points Earned",

                };

                int j = 0;
                foreach (var year in listOfEventYears)
                {
                    j++;
                    chartData[j] = new object[] { ConvertDotNetDateTimeToJavaScriptDate(new DateTime(year, 1, 1)), listOfResultAverages.ElementAt(j - 1) };
                }

                return Json(chartData, JsonRequestBehavior.AllowGet);
            }

        }

        /// <summary>
        /// Converter for TimeSpan to DateTime types.
        /// </summary>
        /// <param name="timeSpan">TimeSpan to convert to a DateTime.</param>
        /// <returns>DateTime representation of the input TimeSpan</returns>
        private DateTime ConvertTimeSpanToDateTime(TimeSpan? timeSpan)
        {
            baseDate = new DateTime();
            baseDate = baseDate.Add((TimeSpan)timeSpan);

            return baseDate;
        }

        /// <summary>
        /// In order to properly send a DotNet DateTime object to JavaScript and convert it to the 
        /// JavaScript Date type there needs to be a converstion to milliseconds based on JavaScripts 
        /// earliest date: 01/01/1970.
        /// </summary>
        /// <param name="dateTimeToConvert">DotNet DateTime to convert to JavaScript DateTime</param>
        /// <returns>JavaScript consumable DotNet DateTime</returns>
        private UInt64 ConvertDotNetDateTimeToJavaScriptDate(DateTime dateTimeToConvert)
        {
            UInt64 msDiff = 0;
            try
            {
                DateTime dotNetDateTime = dateTimeToConvert;
                DateTime dtJSStart = new DateTime(1970, 1, 1);

                if (dateTimeToConvert.Year <= 1969)
                {
                    dotNetDateTime = dateTimeToConvert.AddYears(1969);
                }

                msDiff = Convert.ToUInt64(dotNetDateTime.Subtract(dtJSStart).TotalMilliseconds);
            }
            catch (Exception e)
            {
                throw e;
            }
            return msDiff;
        }


        public bool TestTest()
        {
            return true;
        }

        ////////////////////////////////////////////Tests//////////////////////////////////////////

        public DateTime TestConvertTimeSpanToDateTime(int runDataID)
        {
            TimeSpan timeSpan = (TimeSpan) _runDataRepo.GetAll().Where(rd => rd.RunDataID == runDataID).Select(rd => rd.AdjustedTime).FirstOrDefault();
            baseDate = new DateTime();
            baseDate = baseDate.Add(timeSpan);

            return baseDate;
        }


        public UInt64 TestConvertDotNetDateTimeToJavaScriptDate(int eventID)
        {
            DateTime dateTimeToConvert = _eventsRepo.GetAll().Where(e => e.EventsID == eventID).Select(e => e.EventDate).FirstOrDefault();
            UInt64 msDiff = 0;
            try
            {
                DateTime dotNetDateTime = dateTimeToConvert;
                DateTime dtJSStart = new DateTime(1970, 1, 1);

                if (dateTimeToConvert.Year <= 1969)
                {
                    dotNetDateTime = dateTimeToConvert.AddYears(1969);
                }

                msDiff = Convert.ToUInt64(dotNetDateTime.Subtract(dtJSStart).TotalMilliseconds);
            }
            catch (Exception e)
            {
                throw e;
            }
            return msDiff;
        }
    }
}