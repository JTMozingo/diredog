﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MotoHub.Models.ViewModels
{
    public class QueueViewModel
    {
        public Registration Reg { get; set; }
        public int UserID { get; set; }
        public int EventID { get; set; }
    }
}