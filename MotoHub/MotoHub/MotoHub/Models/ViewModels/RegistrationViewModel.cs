﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MotoHub.Models.ViewModels
{
    public class RegistrationViewModel
    {
        public IEnumerable<Car> Cars { get; set; } //Registration
        public IEnumerable<Event> Event { get; set; }
        public IEnumerable<Registration> Registration { get; set; }
        public IEnumerable<Garage> Garage { get; set; }
        public IEnumerable<Club> Club { get; set; }
        public IEnumerable<UserProfile> UserProfile { get; set; }
        public IEnumerable<CarMake> CarMake { get; set; }
        public IEnumerable<CarClass> CarClass { get; set; }
        public IEnumerable<RunGroup> RunGroup { get; set; }
        public IEnumerable<RunSection> RunSection { get; set; }
        //Not sure if these last two are needed?
        public IEnumerable<EventResult> EventResult { get; set; }
        public IEnumerable<RunData> RunData { get; set; }
    }
}