﻿using MotoHub.DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MotoHub.Models.ViewModels
{
    public class VehicleViewModel
    {
        public IEnumerable<Garage> Garage { get; set; }
        public IEnumerable<Car> Car { get; set; }
        public IEnumerable<CarClass> CarClass { get; set; }
        public IEnumerable<CarMake> CarMake { get; set; }
        public IEnumerable<CarType> CarType { get; set; }
        public IEnumerable<CarColor> CarColor { get; set; }


        //Should eventually hold things like clubs and other user related items from the db.
    }
}