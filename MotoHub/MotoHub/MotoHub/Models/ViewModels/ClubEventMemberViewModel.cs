﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MotoHub.Models.ViewModels
{
    public class ClubEventMemberViewModel
    {
        public IEnumerable<Event> Events { get; set; }
        public IEnumerable<Club> Clubs { get; set; }
        public MotoHubUser MotoHubUser { get; set; }
        public IEnumerable<Membership> Memberships { get; set; }
    }
}