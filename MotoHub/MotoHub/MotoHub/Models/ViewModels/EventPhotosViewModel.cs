﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MotoHub.Models.ViewModels
{
    public class EventPhotosViewModel
    {
        public IEnumerable<Photo> Photos { get; set; }
        public Event Event { get; set; }
    }
}