﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MotoHub.Models.ViewModels
{
    public class ClubPhotosViewModel
    {
        public IEnumerable<Photo> Photos { get; set; }
        public Club Clubs { get; set; }
    }
}