﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MotoHub.Models.ViewModels
{
    public class QRQueueViewModel
    {
        public int Id { get; set; }
        public string Type { get; set; }
        public string QrId { get; set; }
    }
}