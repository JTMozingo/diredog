﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MotoHub.Models.ViewModels
{
    public class QueueAddViewModel
    {
        public string UserID { get; set; }
        public string EventID { get; set; }
    }
}