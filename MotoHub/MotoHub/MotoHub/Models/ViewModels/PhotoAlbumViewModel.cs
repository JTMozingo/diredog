﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MotoHub.Models.ViewModels
{
    public class PhotoAlbumViewModel
    {
        public IEnumerable<Photo> Photos { get; set; }
        public IEnumerable<PhotoAlbum> PhotoAlbums { get; set; }
        public Gallery Gallery { get; set; }
    }
}