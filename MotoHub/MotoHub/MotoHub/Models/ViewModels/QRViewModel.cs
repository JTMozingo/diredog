﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MotoHub.Models.ViewModels
{
    public class QRViewModel
    {
        public string Type { get; set; }
        public string QrId { get; set; }
    }
}