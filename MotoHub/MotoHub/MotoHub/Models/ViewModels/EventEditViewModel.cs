﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace MotoHub.Models.ViewModels
{
    public class EventEditViewModel
    {

        [Key]
        public int EventsID { get; set; }

        [Required]
        [StringLength(64)]
        [Display(Name = "Event Name")]
        public string EventName { get; set; }

        [Required]
        [Column(TypeName = "date")]
        [Display(Name = "Date")]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:MM/dd/yyyy}")]
        [DateIsInFuture(ErrorMessage = "{0} cannot be a date in the past.")]
        public DateTime EventDate { get; set; }

        [Required]
        [StringLength(64)]
        [Display(Name = "Street")]
        public string EventLocationStreet { get; set; }

        [Required]
        [StringLength(64)]
        [Display(Name = "City")]
        public string EventLocationCity { get; set; }

        [Required]
        [StringLength(64)]
        [Display(Name = "State")]
        public string EventLocationState { get; set; }

        [Display(Name = "Zip")]
        public int EventLocationZip { get; set; }

        [AttributeUsage(AttributeTargets.Property | AttributeTargets.Field, AllowMultiple = false)]
        public class DateIsInFuture : ValidationAttribute
        {
            public override bool IsValid(object date)
            {
                DateTime returnDate = (DateTime)date;
                return returnDate > DateTime.Now;
            }
        }
    }
}