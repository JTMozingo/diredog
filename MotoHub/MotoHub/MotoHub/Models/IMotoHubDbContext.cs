﻿using System;
using System.Collections.Generic;
using MotoHub.DAL;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MotoHub.Models
{
    public interface IMotoHubDbContext : IDisposable
    {
        DbSet<Club> Clubs { get; set; }
        DbSet<Committee> Committees { get; set; }
        DbSet<Event> Events { get; set; }
        DbSet<Garage> Garages { get; set; }
        DbSet<Membership> Memberships { get; set; }
        DbSet<MotoHubRole> MotoHubRoles { get; set; }
        DbSet<MotoHubUser> MotoHubUsers { get; set; }
        DbSet<Registration> Registrations { get; set; }
        DbSet<UserProfile> UserProfiles { get; set; }
        DbSet<Photo> Photos { get; set; }
        DbSet<PhotoAlbum> PhotoAlbums { get; set; }
        DbSet<RunData> RunDatas { get; set; }
        DbSet<Gallery> Galleries { get; set; }
        DbSet<Car> Cars { get; set; }
        DbSet<PendingClubInvitation> PendingClubInvitations { get; set; }
        int SaveChanges();
        void MarkAsModified(Club item);
    }
}
