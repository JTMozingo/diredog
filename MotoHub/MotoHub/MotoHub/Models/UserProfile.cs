namespace MotoHub.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("UserProfile")]
    public partial class UserProfile
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int MotoHubUserID { get; set; }

        [Required(ErrorMessage = "We need to know your first name so we know what to call you!")]
        [StringLength(64)]
        public string FirstName { get; set; }

        [Required(ErrorMessage = "We need you last name too! You know...just incase...")]
        [StringLength(64)]
        public string LastName { get; set; }

        [Column(TypeName = "date")]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:dd/MM/yyyy}")]
        [DateIsInPast(ErrorMessage = "Hey...Look I know you're young, but come on. Let's not kid ourselves.")]
        public DateTime? DateOfBirth { get; set; }

        [StringLength(64)]
        public string UserCity { get; set; }

        public string Bio { get; set; }

        public bool ProfileIsPrivate { get; set; }

        public int GalleryID { get; set; }

        public int? ProfilePhotoID { get; set; }

        public virtual Gallery Gallery { get; set; }

        public virtual MotoHubUser MotoHubUser { get; set; }

        public virtual Photo Photo { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<PendingClubInvitation> PendingClubInvitations { get; set; }

        [AttributeUsage(AttributeTargets.Property | AttributeTargets.Field, AllowMultiple = false)]
        public class DateIsInPast : ValidationAttribute
        {
            public override bool IsValid(object date)
            {
                if(date != null)
                {
                    DateTime returnDate = (DateTime)date;
                    return returnDate < DateTime.Now;
                }
                else { return true; }
               
            }
        }
    }
}
