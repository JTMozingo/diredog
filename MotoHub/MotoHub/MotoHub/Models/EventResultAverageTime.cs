﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MotoHub.Models
{
    public partial class EventResultAverageTime
    {
        public TimeSpan AverageTime { get; set; }
        public DateTime EventDate { get; set; }
    }
}