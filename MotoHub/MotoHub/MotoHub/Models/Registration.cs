namespace MotoHub.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Registration")]
    public partial class Registration
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Registration()
        {
            EventResults = new HashSet<EventResult>();
        }

        public int RegistrationID { get; set; }

        public int UserID { get; set; }

        public int EventID { get; set; }

        public int? CarID { get; set; }

        public int? FeesPaid { get; set; }

        public int? ClubsID { get; set; }

        public virtual Car Car { get; set; }

        public virtual Club Club { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<EventResult> EventResults { get; set; }

        public virtual Event Event { get; set; }

        public virtual MotoHubUser MotoHubUser { get; set; }
    }
}
