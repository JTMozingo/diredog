namespace MotoHub.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Membership")]
    public partial class Membership
    {
        [Key]
        [Column(Order = 0)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int MotoHubUserID { get; set; }

        [Key]
        [Column(Order = 1)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int ClubID { get; set; }

        public int RoleID { get; set; }

        public int? CommitteeID { get; set; }

        public bool Status { get; set; }

        public bool DuesPaid { get; set; }

        public virtual Club Club { get; set; }

        public virtual Committee Committee { get; set; }

        public virtual MotoHubRole MotoHubRole { get; set; }

        public virtual MotoHubUser MotoHubUser { get; set; }
    }
}
