namespace MotoHub.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class Photo
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Photo()
        {
            Clubs = new HashSet<Club>();
            Events = new HashSet<Event>();
            UserProfiles = new HashSet<UserProfile>();
        }

        public int PhotoID { get; set; }

        public int PhotoAlbumID { get; set; }

        [Required]
        [StringLength(32)]
        public string PhotoName { get; set; }

        [Required]
        [StringLength(64)]
        public string ContentType { get; set; }

        [Column(TypeName = "image")]
        [Required]
        public byte[] PhotoData { get; set; }

        [StringLength(128)]
        public string PhotoCaption { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Club> Clubs { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Event> Events { get; set; }

        public virtual PhotoAlbum PhotoAlbum { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<UserProfile> UserProfiles { get; set; }
    }
}
