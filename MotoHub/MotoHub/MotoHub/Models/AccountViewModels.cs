﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace MotoHub.Models
{
    public class ExternalLoginConfirmationViewModel
    {
        [Required]
        [Display(Name = "Email")]
        public string Email { get; set; }
    }

    public class ExternalLoginListViewModel
    {
        public string ReturnUrl { get; set; }
    }

    public class SendCodeViewModel
    {
        public string SelectedProvider { get; set; }
        public ICollection<System.Web.Mvc.SelectListItem> Providers { get; set; }
        public string ReturnUrl { get; set; }
        public bool RememberMe { get; set; }
    }

    public class VerifyCodeViewModel
    {
        [Required]
        public string Provider { get; set; }

        [Required]
        [Display(Name = "Code")]
        public string Code { get; set; }
        public string ReturnUrl { get; set; }

        [Display(Name = "Remember this browser?")]
        public bool RememberBrowser { get; set; }

        public bool RememberMe { get; set; }
    }

    public class ForgotViewModel
    {
        [Required]
        [Display(Name = "Email")]
        public string Email { get; set; }
    }

    public class LoginViewModel
    {
        [Required]
        [Display(Name = "Email")]
        [EmailAddress]
        public string Email { get; set; }

        [Required]
        [DataType(DataType.Password)]
        [Display(Name = "Password")]
        public string Password { get; set; }

        [Display(Name = "Remember me?")]
        public bool RememberMe { get; set; }
    }

    public class RegisterViewModel
    {
        public static string currentDate = System.DateTime.Today.ToString();

        [Required]
        [EmailAddress]
        [Display(Name = "Email")]
        public string Email { get; set; }

        [Required]
        [StringLength(100, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Display(Name = "Password")]
        public string Password { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "Confirm password")]
        [Compare("Password", ErrorMessage = "The password and confirmation password do not match.")]
        public string ConfirmPassword { get; set; }

        [Required]
        [DataType(DataType.Text)]
        [Display(Name = "First Name")]
        public string UserFirstName { get; set; }

        [Required]
        [DataType(DataType.Text)]
        [Display(Name = "Last Name")]
        public string UserLastName { get; set; }

        [Required]
        [DataType(DataType.Date)]
        [CustomDate(-100, -13)]
        [Display(Name = "Date of Birth")]
        public DateTime UserDateOfBirth { get; set; }
    }

    public class AdminRegisteredUserViewModel
    {
        public static string currentDate = System.DateTime.Today.ToString();

        [Required]
        [EmailAddress]
        [Display(Name = "Email")]
        public string Email { get; set; }

        [Required]
        [DataType(DataType.Text)]
        [Display(Name = "First Name")]
        public string UserFirstName { get; set; }

        [Required]
        [DataType(DataType.Text)]
        [Display(Name = "Last Name")]
        public string UserLastName { get; set; }

        [Required]
        [DataType(DataType.Date)]
        [CustomDate(-100, -13)]
        [Display(Name = "Date of Birth")]
        public DateTime UserDateOfBirth { get; set; }
    }

    /// <summary>
    /// A method to create custom DateTime Attribute for model building (or
    /// whatever else you want to use it for).
    /// <para name="startingYear">
    /// startingYear: The number of years you wish to remove or add to the current date.
    /// </para>
    /// <para name="endingYear">
    /// endingYear: The number of years you wish to remove or add to the current date.
    /// </para>
    /// </summary>
    /// 
    public class CustomDateAttribute : RangeAttribute
    {
        public CustomDateAttribute(int startingYear, int endingYear)
            : base(typeof(DateTime),
                  
                  DateTime.Now.AddYears(startingYear).ToShortDateString(),
                  DateTime.Now.AddYears(endingYear).ToShortDateString())
        { }
    }

    public class ResetPasswordViewModel
    {
        [Required]
        [EmailAddress]
        [Display(Name = "Email")]
        public string Email { get; set; }

        [Required]
        [StringLength(100, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Display(Name = "Password")]
        public string Password { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "Confirm password")]
        [Compare("Password", ErrorMessage = "The password and confirmation password do not match.")]
        public string ConfirmPassword { get; set; }

        public string Code { get; set; }
    }

    public class ForgotPasswordViewModel
    {
        [Required]
        [EmailAddress]
        [Display(Name = "Email")]
        public string Email { get; set; }
    }
}
