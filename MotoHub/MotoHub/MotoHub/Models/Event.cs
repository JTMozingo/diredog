namespace MotoHub.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class Event
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Event()
        {
            Registrations = new HashSet<Registration>();
        }

        [Key]
        public int EventsID { get; set; }

        public int MotoHubUserID { get; set; }

        public int ClubID { get; set; }

        [Required]
        [StringLength(64)]
        [Display(Name="Event Name")]
        public string EventName { get; set; }

        [Required]
        [Column(TypeName = "date")]
        [Display(Name="Date")]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString="{0:dd/MM/yyyy}")]
        [DateIsInFuture(ErrorMessage = "Ok...so... we are gonna learn about dates today. \nDates must be in the format Day/Month/Year. \nWhen creating your event, you will also need to make sure the date for the event hasn't already passed. \nSo... yeah...Got it now?")]
        public DateTime EventDate { get; set; }

        [Required]
        [StringLength(64)]
        [Display(Name="Street")]
        public string EventLocationStreet { get; set; }

        [Required]
        [StringLength(64)]
        [Display(Name="City")]
        public string EventLocationCity { get; set; }

        [Required]
        [StringLength(64)]
        [Display(Name="State")]
        public string EventLocationState { get; set; }

        [Display(Name="Zip")]
        public int EventLocationZip { get; set; }

        public int? ProfilePhotoID { get; set; }

        public int GalleryID { get; set; }

        public virtual Club Club { get; set; }

        public virtual Gallery Gallery { get; set; }

        public virtual MotoHubUser MotoHubUser { get; set; }

        public virtual Photo Photo { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Registration> Registrations { get; set; }

        [AttributeUsage(AttributeTargets.Property | AttributeTargets.Field, AllowMultiple = false)]
        public class DateIsInFuture : ValidationAttribute
        {
            public override bool IsValid(object date)
            {
                if(date != null)
                {
                    DateTime returnDate = (DateTime)date;
                    return returnDate > DateTime.Now;
                }
                else
                {
                    return false;
                }
            }
        }
    }
}
