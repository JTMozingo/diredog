namespace MotoHub.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("RunSection")]
    public partial class RunSection
    {
        [Key]
        [StringLength(8)]
        public string RunSectionCode { get; set; }

        [StringLength(64)]
        public string RunSectionName { get; set; }
    }
}
