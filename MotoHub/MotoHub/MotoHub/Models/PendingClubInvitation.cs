namespace MotoHub.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class PendingClubInvitation
    {
        [Key]
        [Column(Order = 0)]
        [ForeignKey("UserProfile")]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int MotoHubUserID { get; set; }

        [Key]
        [Column(Order = 1)]
        [ForeignKey("Club")]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int ClubID { get; set; }

        public bool Rejected { get; set; }

        public virtual UserProfile UserProfile { get; set; }
        public virtual Club Club { get; set; }
    }
}
