﻿using MotoHub.DAL;
using MotoHub.Models;
using MotoHub.Repositories.RepositoryInterfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MotoHub.Repositories
{
    public class RunSectionRepository : Repository<RunSection>, IRunSectionRepository
    {
        public RunSectionRepository(MotoHubDbContext context) : base(context)
        {

        }

        public RunSection GetRunSection(string runSectionCode)
        {
            return MotoHubDbContext.RunSections.Where(rs => rs.RunSectionCode == runSectionCode).FirstOrDefault();
        }

        public IEnumerable<RunSection> GetAllRunSections()
        {
            return MotoHubDbContext.RunSections.ToList();
        }

        public int Count()
        {
            return MotoHubDbContext.RunSections.Count();
        }
        
        public MotoHubDbContext MotoHubDbContext
        {
            get { return Context as MotoHubDbContext; }
        }
    }
}