﻿using MotoHub.DAL;
using MotoHub.Models;
using MotoHub.Repositories.RepositoryInterfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MotoHub.Repositories
{
    public class RunGroupRepository : Repository<RunGroup>, IRunGroupRepository
    {
        public RunGroupRepository(MotoHubDbContext context) : base(context)
        {

        }
        
        public RunGroup GetRunGroup(string runGroupCode)
        {
            return MotoHubDbContext.RunGroups.Where(rg => rg.RunGroupCode == runGroupCode).FirstOrDefault();
        }

        public IEnumerable<RunGroup> GetAllRunGroups()
        {
            return MotoHubDbContext.RunGroups.ToList();
        }

        public MotoHubDbContext MotoHubDbContext
        {
            get { return Context as MotoHubDbContext; }
        }
        
    }
}