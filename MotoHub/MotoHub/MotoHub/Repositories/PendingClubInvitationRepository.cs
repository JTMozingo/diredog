﻿using MotoHub.DAL;
using MotoHub.Models;
using MotoHub.Repositories.RepositoryInterfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MotoHub.Repositories
{
    public class PendingClubInvitationRepository : Repository<PendingClubInvitation>, IPendingClubInvitationsRepository
    {

        public PendingClubInvitationRepository(MotoHubDbContext context) : base(context)
        {

        }

        public PendingClubInvitation GetPendingClubInvitation(int userID, int clubID)
        {
            return MotoHubDbContext.PendingClubInvitations.Where(pci => pci.ClubID == clubID).Where(pci => pci.MotoHubUserID == userID).FirstOrDefault();
        }

        public MotoHubDbContext MotoHubDbContext
        {
            get { return Context as MotoHubDbContext; }
        }
    }
}