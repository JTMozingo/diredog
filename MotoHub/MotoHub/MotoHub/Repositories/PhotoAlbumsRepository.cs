﻿using MotoHub.DAL;
using MotoHub.Models;
using MotoHub.Repositories.RepositoryInterfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MotoHub.Repositories
{
    public class PhotoAlbumsRepository : Repository<PhotoAlbum>, IPhotoAlbumsRepository
    {
        public PhotoAlbumsRepository(MotoHubDbContext context) : base(context)
        {
        }

        public PhotoAlbum GetPhotoAlbum(int photoAlbumID)
        {
            return MotoHubDbContext.PhotoAlbums.Where(pa => pa.PhotoAlbumID == photoAlbumID).FirstOrDefault();
        }

        public MotoHubDbContext MotoHubDbContext
        {
            get { return Context as MotoHubDbContext; }
        }
    }
}