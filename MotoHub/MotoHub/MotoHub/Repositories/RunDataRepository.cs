﻿using MotoHub.DAL;
using MotoHub.Models;
using MotoHub.Repositories.RepositoryInterfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MotoHub.Repositories
{
    public class RunDataRepository : Repository<RunData>, IRunDataRepository
    {
        public RunDataRepository(MotoHubDbContext context) : base(context)
        {

        }
        
        public RunData GetRunData(int runDataID)
        {
            return MotoHubDbContext.RunDatas.Where(rd => rd.RunDataID == runDataID).FirstOrDefault();
        }

        public void Create(int eventResultsID)
        {
            RunData newEntry = new RunData()
            {
                EventResultsID = eventResultsID,
                RunNumber = 1
            };

            MotoHubDbContext.RunDatas.Add(newEntry);
            MotoHubDbContext.SaveChanges();
        }

        public IEnumerable<RunData> GetAllRunDataFromEventResults(IEnumerable<EventResult> eventResults)
        {
            IEnumerable<RunData> runDataFromEventResults = new List<RunData>();

            foreach (var item in eventResults)
            {
                runDataFromEventResults = runDataFromEventResults.Concat(item.RunDatas);
            }

            return runDataFromEventResults;
        }

        /// <summary>
        /// Gets the best adjusted times from a list of runData.
        /// </summary>
        /// <param name="runData">List of RunData to choose the best Adjusted Times from.</param>
        /// <param name="eventResult">List of EventResults to compare against the RunData</param>
        /// <returns>List of RunData with the lowest (best for races) adjested times.</returns>
        public IEnumerable<RunData> GetBestAdjustedTimeFromRunData(IEnumerable<RunData> runData, IEnumerable<EventResult> eventResult)
        {
            List<RunData> bestTimeFromEventResult = new List<RunData>();

            foreach (var eventResultItem in eventResult)
            {
                IEnumerable<RunData> runDataForCurrentEventResultItem = runData.Where(rd => rd.EventResultsID == eventResultItem.EventResultsID).ToList();
                RunData bestAdjustedTime = new RunData();

                foreach (var runDataItem in runDataForCurrentEventResultItem)
                {
                    if (bestAdjustedTime.AdjustedTime > runDataItem.AdjustedTime || bestAdjustedTime.AdjustedTime == null)
                    {
                        bestAdjustedTime = runDataItem;
                    }
                }

                bestTimeFromEventResult.Add(bestAdjustedTime);
            };

            return bestTimeFromEventResult;
        }

        public IEnumerable<EventResultAverageTime> GetAverageAdjustedTimeFromRunData(IEnumerable<RunData> runData, IEnumerable<EventResult> eventResult)
        {
            //List<TimeSpan> averageAdjustedRunTimeList = new List<TimeSpan>();
            IEnumerable<EventResult> orderedEventResultsByOldestEvent = eventResult.OrderBy(er => er.Registration.Event.EventDate);
            List<EventResultAverageTime> averageAdjustedTimesForEventResult = new List<EventResultAverageTime>();

            foreach (var eventResultItem in orderedEventResultsByOldestEvent)
            {
                IEnumerable<RunData> runDataForCurrentEventResultItem = runData.Where(rd => rd.EventResultsID == eventResultItem.EventResultsID).ToList();
                TimeSpan averageAdjustedTime = GetAverageAdjustedTime(runDataForCurrentEventResultItem);

                averageAdjustedTimesForEventResult.Add(new EventResultAverageTime
                {
                    AverageTime = averageAdjustedTime,
                    EventDate = eventResultItem.Registration.Event.EventDate
                });
            }

            return averageAdjustedTimesForEventResult;
        }

        private TimeSpan GetAverageAdjustedTime(IEnumerable<RunData> runData)
        {
            List<TimeSpan> allAdjustedTimesFromRunData = new List<TimeSpan>();

            foreach (var runDataItem in runData)
            {
                allAdjustedTimesFromRunData.Add((TimeSpan)runDataItem.AdjustedTime);
            }

            double doubleAverageTicks = allAdjustedTimesFromRunData.Average(list => list.Ticks);
            long longAverageTicks = Convert.ToInt64(doubleAverageTicks);

            TimeSpan averageAdjustedTime = new TimeSpan(longAverageTicks);

            return averageAdjustedTime;
        }
        /// <summary>
        /// Get the Previous RunData entry from the one entered
        /// </summary>
        /// <param name="runDataID"></param>
        /// <returns>The RunData row </returns>
        public RunData GetPreviousRun(int runDataID)
        {
            return MotoHubDbContext.RunDatas.Where(x => x.RunDataID < runDataID)
                                            .OrderByDescending(x => x.RunDataID)
                                            .FirstOrDefault();

        }

        public RunData GetNextRun(int runDataID)
        {
            return MotoHubDbContext.RunDatas.Where(x => x.RunDataID > runDataID)
                                            .OrderBy(x => x.RunDataID)
                                            .FirstOrDefault();
        }

        public MotoHubDbContext MotoHubDbContext
        {
            get { return Context as MotoHubDbContext; }
        }

        public void CreateFromUserAndEvent(int userID, int eventID)
        {
            Registration reg = MotoHubDbContext.Registrations.Where(r => r.UserID == userID && r.EventID == eventID).FirstOrDefault();
            EventResult eventresults = MotoHubDbContext.EventResults.Where(e => e.RegistrationID == reg.RegistrationID).FirstOrDefault();
            Create(eventresults.EventResultsID);
        }

        public void Create(int eventResultsID, int runNumber)
        {
            throw new NotImplementedException();
        }
    }
}