﻿using MotoHub.DAL;
using MotoHub.Models;
using MotoHub.Repositories.RepositoryInterfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MotoHub.Repositories
{
    public class MembershipRepository : Repository<Membership>, IMembershipRepository
    {
        public MembershipRepository(MotoHubDbContext context) : base(context)
        {

        }

        public Membership GetMembership(int userID, int clubID)
        {
            return MotoHubDbContext.Memberships.Where(m => m.ClubID == clubID)
                                               .Where(m => m.MotoHubUserID == userID)
                                               .FirstOrDefault();
        }

        public Membership GetClubMembership(int clubID, int userID)
        {
            return MotoHubDbContext.Memberships.Where(m => m.MotoHubUserID == userID)
                                               .Where(m => m.ClubID == clubID)
                                               .FirstOrDefault();
        }

        public MotoHubDbContext MotoHubDbContext
        {
            get { return Context as MotoHubDbContext; }
        }
    }
}