﻿using MotoHub.DAL;
using MotoHub.Models;
using MotoHub.Repositories.RepositoryInterfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MotoHub.Repositories
{
    public class RegistrationRepository : Repository<Registration>, IRegistrationRepository
    {
        public RegistrationRepository(MotoHubDbContext context) : base(context)
        {

        }

        public Registration GetRegistration(int registrationID)
        {
            return MotoHubDbContext.Registrations.Where(r => r.RegistrationID == registrationID).FirstOrDefault();
        }

        public IEnumerable<Registration> GetAllUserRegistrations(int userID)
        {
            return MotoHubDbContext.Registrations.Where(r => r.UserID == userID).ToList();
        }

        public Registration GetUserRegistrationForEvent(int userID, int eventID)
        {
            return MotoHubDbContext.Registrations.Where(r => r.UserID == userID && r.EventID == eventID).FirstOrDefault();
        }

        public MotoHubDbContext MotoHubDbContext
        {
            get { return Context as MotoHubDbContext; }
        }
    }
}