﻿using MotoHub.DAL;
using MotoHub.Models;
using MotoHub.Repositories.RepositoryInterfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MotoHub.Repositories
{
    public class ClubRepository : Repository<Club>, IClubRepository
    {
        public ClubRepository(MotoHubDbContext context) : base(context)
        {

        }

        public Club GetClub(int clubID)
        {
            return MotoHubDbContext.Clubs.Where(c => c.ClubsID == clubID).FirstOrDefault();
        }

        /// <summary>
        /// Returns the club that is holding the event linked to the passed in eventID.
        /// </summary>
        /// <param name="eventID">Event to search by.</param>
        /// <returns>Club holding the event.</returns>
        public Club GetClubHoldingEvent(int eventID)
        {
            return MotoHubDbContext.Clubs.Where(c => c.Events.Contains(MotoHubDbContext.Events
                                                                       .Where(e => e.EventsID == eventID)
                                                                       .FirstOrDefault())) 
                                         .FirstOrDefault();
        }

        /// <summary>
        /// Checks if the input user holds the role of ClubAdmin to the input club.
        /// </summary>
        /// <param name="motoHubUserID">Member to check</param>
        /// <param name="clubID">club to check.</param>
        /// <returns>Boolean</returns>
        public bool IsAdminOfClub(int motoHubUserID, int? clubID)
        {
            MotoHubRole role = MotoHubDbContext.MotoHubRoles.Where(mr => mr.RoleTitle == "ClubAdmin").FirstOrDefault();
            Membership membershipFound = MotoHubDbContext.Memberships.Where(m => m.ClubID == clubID)
                                                                     .Where(m => m.MotoHubUserID == motoHubUserID)
                                                                     .Where(m => m.RoleID == role.RoleID)
                                                                     .FirstOrDefault();
            if(membershipFound != null)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public IEnumerable<Club> GetAllClubsFromListOfRegistrations(IEnumerable<Registration> registrations)
        {
            List<Club> clubs = new List<Club>();
            foreach(var registration in registrations)
            {
                clubs.Add(MotoHubDbContext.Clubs.Where(c => c.ClubsID == registration.ClubsID).FirstOrDefault());
            }
            return clubs;
        }

        public MotoHubDbContext MotoHubDbContext
        {
            get { return Context as MotoHubDbContext; }
        }
    }
}