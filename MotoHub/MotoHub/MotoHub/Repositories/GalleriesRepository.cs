﻿using MotoHub.DAL;
using MotoHub.Models;
using MotoHub.Repositories.RepositoryInterfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MotoHub.Repositories
{
    public class GalleriesRepository : Repository<Gallery>, IGalleriesRepository
    {
        public GalleriesRepository(MotoHubDbContext context) : base(context)
        {

        }

        public Gallery GetGallery(int galleryID)
        {
            return MotoHubDbContext.Galleries.Where(g => g.GalleryID == galleryID).FirstOrDefault();
        }

        public MotoHubDbContext MotoHubDbContext
        {
            get { return Context as MotoHubDbContext; }
        }
    }
}