﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MotoHub.Repositories.RepositoryInterfaces;

namespace MotoHub.Repositories.Persistance
{
    interface IUnitOfWork : IDisposable
    {
        IMotoHubUserRepository MotoHubUsers { get; }
        IEventRepository Events { get; }
        IClubRepository Clubs { get; }
        IMembershipRepository Memberships { get; }
        IRegistrationRepository Registrations { get; }
        IRoleRepository Roles { get; }
        IRunGroupRepository RunGroups { get; }
        IRunSectionRepository RunSections { get; }
        IRunDataRepository RunData { get; }
        IEventResultsRepository EventResults { get; }
        ICarClassRepository CarClasses { get; }
        ICarRepository Cars { get; }
        int Complete();
    }
}
