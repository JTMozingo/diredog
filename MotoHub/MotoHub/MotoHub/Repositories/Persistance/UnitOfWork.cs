﻿using MotoHub.DAL;
using MotoHub.Repositories.RepositoryInterfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MotoHub.Repositories.Persistance
{
    public class UnitOfWork : IUnitOfWork
    {
        private readonly MotoHubDbContext _context;

        public UnitOfWork(MotoHubDbContext context)
        {
            _context = context;
            MotoHubUsers = new MotoHubUserRepository(_context);
            Events = new EventRepository(_context);
            Clubs = new ClubRepository(_context);
            Memberships = new MembershipRepository(_context);
            Registrations = new RegistrationRepository(_context); 
            Roles = new RoleRepository(_context);
            RunGroups = new RunGroupRepository(_context);
            RunSections = new RunSectionRepository(_context);
            RunData = new RunDataRepository(_context);
            EventResults = new EventResultsRepository(_context);
            CarClasses = new CarClassRepository(_context);
            Cars = new CarRepository(_context);
        }

        public IMotoHubUserRepository MotoHubUsers { get; private set; }
        public IEventRepository Events { get; private set; }
        public IClubRepository Clubs { get; private set; }
        public IMembershipRepository Memberships { get; private set; }
        public IRegistrationRepository Registrations { get; private set; }
        public IRoleRepository Roles { get; private set; }
        public IRunGroupRepository RunGroups { get; private set; }
        public IRunSectionRepository RunSections { get; private set; }
        public IRunDataRepository RunData { get; private set; }
        public IEventResultsRepository EventResults { get; private set; }
        public ICarClassRepository CarClasses { get; private set; }
        public ICarRepository Cars { get; private set; }
        ///More repositories go here.

        public int Complete()
        {
            return _context.SaveChanges();
        }

        public void Dispose()
        {
            _context.Dispose();
        }
    }
}