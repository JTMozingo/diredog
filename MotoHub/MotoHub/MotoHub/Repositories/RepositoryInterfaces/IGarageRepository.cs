﻿using MotoHub.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MotoHub.Repositories.RepositoryInterfaces
{
    public interface IGarageRepository : IRepository<Garage>
    {
        Garage GetGarage(int garageID);
    }
}
