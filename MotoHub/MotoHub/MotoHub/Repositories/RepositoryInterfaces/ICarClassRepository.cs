﻿using MotoHub.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MotoHub.Repositories.RepositoryInterfaces
{
    public interface ICarClassRepository : IRepository<CarClass>
    {
        CarClass GetCarClass(int carClassID);
        IEnumerable<CarClass> GetAllCarClasses();
        int GetCarClassIDFromCarClassName(string carClassName);
        
    };
}
