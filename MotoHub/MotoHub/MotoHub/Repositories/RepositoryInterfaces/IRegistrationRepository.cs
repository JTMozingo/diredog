﻿using MotoHub.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MotoHub.Repositories.RepositoryInterfaces
{
    public interface IRegistrationRepository : IRepository<Registration>
    {
        Registration GetRegistration(int registrationID);
        Registration GetUserRegistrationForEvent(int userID, int eventID);
        IEnumerable<Registration> GetAllUserRegistrations(int userID);
    }
}
