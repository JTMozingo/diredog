﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using MotoHub.Models;

namespace MotoHub.Repositories.RepositoryInterfaces
{
    public interface ICarRepository : IRepository<Car>
    {
    }
}