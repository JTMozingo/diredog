﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MotoHub.Models;

namespace MotoHub.Repositories.RepositoryInterfaces
{
    public interface IEventRepository : IRepository<Event>
    {
        Event GetEvent(int eventID);
        IEnumerable<Event> GetAllEventsFromListOfRegistrations(IEnumerable<Registration> registrations);
    }
}
