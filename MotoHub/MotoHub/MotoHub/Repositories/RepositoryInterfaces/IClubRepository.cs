﻿using MotoHub.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MotoHub.Repositories.RepositoryInterfaces
{
    public interface IClubRepository : IRepository<Club>
    {
        Club GetClub(int clubID);
        Club GetClubHoldingEvent(int eventID);
        bool IsAdminOfClub(int motoHubUserID, int? clubID);
        IEnumerable<Club> GetAllClubsFromListOfRegistrations(IEnumerable<Registration> registrations);
    }
}
