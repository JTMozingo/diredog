﻿using MotoHub.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MotoHub.Repositories.RepositoryInterfaces
{
    public interface IEventResultsRepository : IRepository<EventResult>
    {
        EventResult GetResult(int eventResultID);
        EventResult Create(int registrationID, int carClass, string runGroupCode, string runSectionCode);
        IEnumerable<EventResult> GetAllEventResultsFromEvent(int eventID);
        IEnumerable<EventResult> GetAllPastEventResultsFromCurrentUser(MotoHubUser currentUser, IEventResultsRepository _eventResultsRepo);
        IEnumerable<int> GetAllYearsForEventsParticipatedIn(IEnumerable<EventResult> resultsToGetEventYearsOf);
        IEnumerable<EventResult> GetAllEventResultsFromYear(IEnumerable<EventResult> eventResultsToFilter, int year);
        double? GetAveragePointsEarned(IEnumerable<EventResult> eventResults);
        EventResult GetEventResultForUser(Event @event, int userID);
    }
}
