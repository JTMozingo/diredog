﻿using MotoHub.DAL;
using MotoHub.Models;
using MotoHub.Repositories.RepositoryInterfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MotoHub.Repositories
{
    public class PhotosRepository : Repository<Photo>, IPhotosRepository
    {
        public PhotosRepository(MotoHubDbContext context) : base(context)
        {

        }

        public Photo GetPhoto(int photoID)
        {
            throw new NotImplementedException();
        }

        public MotoHubDbContext MotoHubDbContext
        {
            get { return Context as MotoHubDbContext; }
        }
    }
}