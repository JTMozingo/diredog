﻿using Microsoft.AspNet.Identity;
using MotoHub.DAL;
using MotoHub.Models;
using MotoHub.Repositories.RepositoryInterfaces;
using System;
using System.Security;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MotoHub.Repositories
{
    public class EventResultsRepository : Repository<EventResult>, IEventResultsRepository
    {
        public EventResultsRepository(MotoHubDbContext context) : base(context)
        {

        }

        public EventResult GetResult(int eventResultID)
        {
            return MotoHubDbContext.EventResults.Where(er => er.EventResultsID == eventResultID).FirstOrDefault();
        }

        public EventResult Create(int registrationID, int carClass, string runGroupCode, string runSectionCode)
        {
            EventResult newEventResult = new EventResult()
            {
                RegistrationID = registrationID,
                CarClassID = carClass,
                RunGroupCode = runGroupCode,
                RunSectionCode = runSectionCode,
                Points = 0,

            };

            MotoHubDbContext.EventResults.Add(newEventResult);
            MotoHubDbContext.SaveChanges();

            return newEventResult;
        }
        
        public IEnumerable<EventResult> GetAllEventResultsFromEvent(int eventID)
        {
            return MotoHubDbContext.EventResults.Where(er => er.Registration.EventID == eventID).ToList();
        }

        public IEnumerable<EventResult> GetAllPastEventResultsFromCurrentUser(MotoHubUser currentUser, IEventResultsRepository _eventResultsRepo)
        {

            IEnumerable<EventResult> userEventResults = _eventResultsRepo.GetAll().Where(er => er.Registration.UserID == currentUser.MotoHubUserID)
                                                                                  .Where(er => er.Registration.Event.EventDate < System.DateTime.Now)
                                                                                  .ToList();
            return userEventResults;
        }

        /// <summary>
        /// Gets all of the years from the EventDate property of an Event Table associated with an EventResult that is passed in.
        /// These years are stored as ints and then returned in as an IEnumerable<int> type.
        /// </summary>
        /// <param name="resultsToGetEventYearsOf"></param>
        /// <returns>List of years</returns>
        public IEnumerable<int> GetAllYearsForEventsParticipatedIn(IEnumerable<EventResult> resultsToGetEventYearsOf)
        {
            List<int> eventYears = new List<int>();
            foreach (var resultItem in resultsToGetEventYearsOf)
            {
                int eventYear = resultItem.Registration.Event.EventDate.Year;

                if(!eventYears.Contains(eventYear))
                {
                    eventYears.Add(eventYear);
                }
            }
            return eventYears;
        }

        public IEnumerable<EventResult> GetAllEventResultsFromYear(IEnumerable<EventResult> eventResultsToFilter, int year)
        {
            List<EventResult> filteredEventResults = new List<EventResult>();

            foreach (var resultItem in eventResultsToFilter)
            {
                if(resultItem.Registration.Event.EventDate.Year == year)
                {
                    filteredEventResults.Add(resultItem);
                }
            }

            return filteredEventResults;
        }

        public double? GetAveragePointsEarned(IEnumerable<EventResult> eventResults)
        {
            return eventResults.Average(er => er.Points);
        }

        public EventResult GetEventResultForUser(Event @event, int userID)
        {
            return MotoHubDbContext.EventResults.Where(er => er.Registration.EventID == @event.EventsID)
                                         .Where(er => er.Registration.MotoHubUser.MotoHubUserID == userID).FirstOrDefault();
        }
        
        public MotoHubDbContext MotoHubDbContext
        {
            get { return Context as MotoHubDbContext; }
        }
    }
}