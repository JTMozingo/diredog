﻿let pastEventsChevron = document.getElementById("past-events-chevron");
let upcomingEventsChevron = document.getElementById("upcoming-events-chevron");
let upcomingEventsTable = document.getElementById('upcoming-events-table');
let pastEventsTable = document.getElementById('past-events-table');
let membershipInvitationModalContainer = document.getElementById('membership-invitation-modal-container');

upcomingEventsChevron.style.cursor = "pointer";
pastEventsChevron.style.cursor = "pointer";

upcomingEventsChevron.addEventListener('click', showUpcomingEventsTable, false);
pastEventsChevron.addEventListener('click', showPastEventsTable, false);

function showUpcomingEventsTable(e) {
    $('#upcoming-events-table-container').toggle("show");
};

function showPastEventsTable(e) {
    $('#past-events-table-container').toggle("show");

};


$("#toggle-invitation-modal").on("click", function (e) {
    StopDefaults(e);
    GetMembershipPartial();
});

function AcceptClubInvitation(ev, clubsID) {
    StopDefaults(ev);

    $.ajax({

        type: 'POST',
        url: "/Profile/AddUserToClubWithoutRedirect",
        data: { clubID: clubsID },
        dataType: 'html',
        success: function (response) {
            GetMembershipPartial();
        },
        error:function() {
            console.log("Some error in ajax.")
        }
    });
}

function rejectClubInvitation(ev, clubsID) {
    StopDefaults(ev);

    $.ajax({
        type: 'POST',
        url: "/Profile/RejectMembershipInvitation",
        data: { clubID: clubsID },
        dataType: 'html',
        success: function (response) {
            GetMembershipPartial();
        },
        error: function () {
            console.log("Some error in ajax.")
        }
    });
}

function GetMembershipPartial() {
    
    $.ajax({

        type: 'GET',
        url: "/Profile/GetMembershipInvitationPartial",
        dataType: 'html',
        success: function (response) {
            console.log("success!");
            $('#club-invitation-modal').modal("hide");
            $('.modal-backdrop').remove();
            $('#membership-invitation-modal-container').html("");
            $('#membership-invitation-modal-container').html(response);
            $('#membership-invitation-modal-container').toggle(true);
            $('#club-invitation-modal').modal("show");
        },
        error: function () {
            console.log("Some error in ajax.");
        }

    });
}

function StopDefaults(e) {
    e.preventDefault();
    e.stopPropagation();
}