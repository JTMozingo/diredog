﻿var route = "";

function openQRCamera(node) {
    var reader = new FileReader();
    reader.onload = function () {
        node.value = "";
        var decodeString = "";
        qrcode.callback = function (res) {
            if (res instanceof Error) {
                alert("No QR code found. Please make sure the QR code is within the camera's frame and try again.");
            } else {
                console.log(res);
                decodeString = res;
                parseQR(decodeString);
            }
        };
        qrcode.decode(reader.result);
    };
    reader.readAsDataURL(node.files[0]);
}

function parseQR(id) {
    var idRegex = /(^[A-Z])(\d+)/;
    var idMatch = idRegex.exec(id);

    var jsonReturn = {
        "Type": idMatch[1],
        "QrId": idMatch[2]
    }
    getPartial(jsonReturn);
}

function getPartial(obj) {
    $.ajax({
        url: route,
        type: 'POST',
        contentType: 'application/json; charset=utf-8',
        data: JSON.stringify(obj),
        dataType: 'html',
        success: function (data) {
            $("#output").html(data);
        }
    });
}
//could have a variable,
//then set the variable in the callback, 
//and then return the variable