﻿$(document).ready(function () {

});


function getVehicleByMake() {
    var make = $('#CarMake');
     console.log(make.val());
     var source ="https://vpic.nhtsa.dot.gov/api/vehicles/getmodelsformake/"+ make.val() + "?format=json";
    console.log(source);
    $.ajax({
        url: source,
        type: "GET",
        dataType: "json",
        success: function (result) {

            console.log(result);
            $('#Model').empty();

            if (result.Results.length == 0) {
                $('#Model').append(' <option disabled selected > -- No Vehicle For Search -- </option>');
            }
            for (var i = 0; i < result.Results.length; i++) {
                var vehicle = result.Results[i].Model_Name;
                $('#Model').append('<option value ="' + vehicle + '">' + vehicle + '</option>');
            }


        },
        error: function (xhr, ajaxOptions, thrownError) {
            console.log(xhr.status);
            console.log(thrownError);
        }
    });
}


function getCarType() {
    var select = $('#CarType');
    var make = $('#Make');

    var source = "https://vpic.nhtsa.dot.gov/api/vehicles/GetVehicleTypesForMake/" + make.val() + "?format=json";
    console.log(source);

    $.ajax({
        url: source,
        type: "GET",
        dataType: "json",
        success: function (result) {
            console.log(result);


            if (result.Results.length == 0) {
                $('#CarType').append(' <option disabled selected > -- No Vehicle Type Found -- </option>');
            }
            else {
               // $('#CarType').empty();

                for (var i = 0; i < result.Results.length; i++) {
                    var type = result.Results[i].VehicleTypeName;
                    $('#CarType').append('<option value ="' + type + '">' + type + '</option>');
                }
            }
        },

        error: function (xhr, ajaxOptions, thrownError) {
            console.log(xhr.status);
            console.log(thrownError);
        }


    });
}


function saveCar() {
    var make = $('#Make').val();
    var model = $('#Model').val();
    var year = $('#year').val();
    var type = $('#CarType').val();

    $('#carSaver').append(' ' + year + ' ' + make + ' ' + model + "<br />");
}
