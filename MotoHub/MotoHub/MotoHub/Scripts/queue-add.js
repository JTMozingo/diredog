﻿$('#addButton').click(function () { //On click of your button

    var userID = $('#UserID').val(); //Get the values from the page you want to post
    var eventID = $('#EventID').val();


    var JSONObject = { // Create JSON object to pass through AJAX
        UserID: userID, //Make sure these names match the properties in VM
        EventID: eventID
    };

    $.ajax({ //Do an ajax post to the controller
        type: 'POST',
        url: '/Events/GetQueuePartial/',
        data: JSON.stringify(JSONObject),
        contentType: "application/json; charset=utf-8",
        dataType: "json"
    });
});