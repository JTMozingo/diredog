﻿

// Load the Visualization API and the corechart package.
google.charts.load('current', {'packages':['line', 'corechart']});
// Set a callback to run when the Google Visualization API is loaded.
google.charts.setOnLoadCallback(drawChart);

// Callback that creates and populates a data table,
// instantiates the pie chart, passes in the data and
// draws it.
function drawChart() {

    drawBestAdjustedTimesChart();
    drawAverageAdjustedTimesChart();
    drawAveragePointsChart();
}

function drawAveragePointsChart() {

    var jsonData;
    $.ajax({
        url: "/Chart/GetAveragePointsForTheYear",
        success: function (response) {
            jsonData = response;
            /*var test = Object.keys(jsonData).map(function (key) {
                return [Number(key), jsonData[key]];
            });*/
            
            $.each(jsonData, function (i, item) {
                if (i != 0) {
                    jsonData[i] = [new Date(item[0]), item[1]]
                }
            });
            
            var options = {
                title: 'Average Points Per Year',
                hAxis: {
                    title: 'Year',
                    format: 'yyyy',
                    gridlines: { count: jsonData.length}
                },
                vAxis: {
                    title:'Points',
                    viewWindow: {
                        min: 0
                    }
                }
                
            }

            var data = new google.visualization.arrayToDataTable(jsonData);

            // Instantiate and draw our chart, passing in some options.
            var chart = new google.visualization.ColumnChart(document.getElementById('average-points-per-year-chart'));
            chart.draw(data, options);
        },
        error: function () {
            console.log("Error in AJAX")
        }
    });
}

function drawAverageAdjustedTimesChart() {
    var data = new google.visualization.DataTable();
    data.addColumn('date', 'Date');
    data.addColumn('date', 'Average Adjusted Time');

    $.getJSON("/Chart/GetAverageAdjustedTimeChart", null, function (chartData) {
        $.each(chartData, function (i, item) {
            data.addRow([new Date(item.eventDate), new Date(item.convertedTimes)]);
        });

        // Set chart options
        var options = {
            title: 'Average Adjusted Run Times',
            width: 800,
            height: 500,
            theme: 'material',
            hAxis: {
                format: 'M/dd/yyyy',
                gridlines: { count: 20 }
            },
            vAxis: {
                format: 'mm:ss.SS',
                gridlines: { count: 20 }
            },
            lineWidth: 10,
            pointSize: 10
        };

        // Instantiate and draw our chart, passing in some options.
        var chart = new google.charts.Line(document.getElementById('average-adjusted-time-chart-div'));
        chart.draw(data, google.charts.Line.convertOptions(options));
    });
}

function drawBestAdjustedTimesChart() {
    // Create the data table.
    var data = new google.visualization.DataTable();
    data.addColumn('date', 'Date');
    data.addColumn('date', 'Adjusted Time');
    $.getJSON("/Chart/GetAdjustedRunTimeProgressionOverTimeChart", null, function (chartData) {
        $.each(chartData, function (i, item) {
            data.addRow([new Date(item.eventDate), new Date(item.convertedTimes)]);
        });

        // Set chart options
        var options = {
            title: 'Best Adjusted Run Times',
            width: 800,
            height: 500,
            theme: 'material',
            hAxis: {
                format: 'M/dd/yyyy',
                gridlines: { count: 20 }
            },
            vAxis: {
                format: 'mm:ss.SS',
                gridlines: { count: 20 }
            },
            lineWidth: 10,
            pointSize: 10
        };

        // Instantiate and draw our chart, passing in some options.
        var chart = new google.charts.Line(document.getElementById('chart_div'));
        chart.draw(data, google.charts.Line.convertOptions(options));
    });
}