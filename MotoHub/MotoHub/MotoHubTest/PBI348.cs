﻿using System;
using System.Text;
using System.Collections.Generic;
using NUnit.Framework;
using Moq;
using MotoHub.Repositories.RepositoryInterfaces;
using MotoHub.Controllers;
using MotoHub.Models;
using System.Linq;

namespace MotoHubTest
{
    /// <summary>
    /// Summary description for PBI358
    /// </summary>
    [TestFixture]
    public class PBI348
    {
        private List<Event> events;
        private List<Club> clubs;
        private List<Membership> memberships;
        private List<MotoHubRole> roles;
        private List<MotoHubUser> users;
        private List<Registration> registrations;
        private List<EventResult> eventResults;
        private List<RunData> runData;

        private Mock<IEventResultsRepository> _eventResults = new Mock<IEventResultsRepository>();
        private Mock<IMotoHubUserRepository> _motoUser = new Mock<IMotoHubUserRepository>();
        private Mock<IRunDataRepository> _runData = new Mock<IRunDataRepository>();
        private Mock<IEventRepository> _events = new Mock<IEventRepository>();

        private ChartController chartController;

        [SetUp]
        public void SetUp()
        {
            events = new List<Event>()
            {
                new Event()
                {
                    EventsID = 8573,
                    ClubID = 10029,
                    MotoHubUserID = 73,
                    EventDate = new DateTime(2016, 2, 10),
                    EventName = "Stuffs Happening",
                    EventLocationStreet = "1474 Elo Street",
                    EventLocationCity = "Eugene",
                    EventLocationState = "OR",
                    EventLocationZip = 97461,
                    GalleryID = 44642
                }
            };

            clubs = new List<Club>()
            {
                new Club()
                {
                    ClubsID = 10029,
                    ClubName = "Test Club",
                    GalleryID = 223
                }
            };

            users = new List<MotoHubUser>()
            {
                new MotoHubUser()
                {
                    MotoHubUserID = 73,
                    AspNetIdentityID = "who knows"
                }
            };

            roles = new List<MotoHubRole>()
            {
                new MotoHubRole()
                {
                    RoleID = 1,
                    RoleTitle = "ClubAdmin"
                }
            };

            memberships = new List<Membership>()
            {
                new Membership()
                {
                    ClubID = 10029,
                    MotoHubUserID = 73,
                    RoleID = 1,
                    Status = true
                }
            };

            registrations = new List<Registration>()
            {
                new Registration()
                {
                    RegistrationID = 34,
                    UserID = 73,
                    EventID = 8573
                }
            };

            eventResults = new List<EventResult>()
            {
                new EventResult()
                {
                    EventResultsID = 20,
                    RegistrationID = 34,
                    RunGroupCode = "AM",
                    CarClassID = 1
                }
            };

            runData = new List<RunData>()
            {
                new RunData()
                {
                    RunDataID = 55,
                    EventResultsID = 20,
                    RunNumber = 1,
                    RawTime = new TimeSpan(0,0,5,23,5),
                    AdjustedTime = new TimeSpan(0,0,5,23,5),
                }
            };
        }

        /// <summary>
        /// Test for PBI 348
        /// </summary>
        [Test]
        public void Chart_CheckThatDateTimesAreCorrectlyConvertedToJavaScriptDateFormat_ReturnsTrue()
        {
            _events.Setup(e => e.GetAll()).Returns(events);

            chartController = new ChartController(_events.Object);
            UInt64 result = chartController.TestConvertDotNetDateTimeToJavaScriptDate(8573);

            Assert.IsTrue(result.Equals(1455062400000));
        }

        
        /// <summary>
        /// Test for PBI 348
        /// </summary>
        [Test]
        public void Chart_TestTimeSpanConversionToDateTime()
        {
            _runData.Setup(rd => rd.GetAll()).Returns(runData);

            chartController = new ChartController(_runData.Object);

            DateTime result = chartController.TestConvertTimeSpanToDateTime(55);
            DateTime shouldBe = new DateTime().Add((TimeSpan)_runData.Object.GetAll().Where(rd => rd.RunDataID == 55).Select(rd => rd.AdjustedTime).FirstOrDefault());

            Assert.AreEqual(result, shouldBe);
        }
    }
}
