﻿using NUnit.Framework;
using MotoHub.Controllers;
using MotoHub.Models;
using Moq;
using System;
using MotoHub.Repositories.RepositoryInterfaces;
using System.Collections.Generic;
using System.Web.Mvc;
using MotoHub.Repositories;

namespace MotoHubTest
{
    [TestFixture]
    public class EventTesting
    {
        private List<Event> events;
        private List<Club> clubs;
        private List<Membership> memberships;
        private List<MotoHubRole> roles;
        private List<MotoHubUser> users;

        private Mock<IEventRepository> mockEventRepo;
        private Mock<IClubRepository> mockClubRepo;
        private Mock<IMembershipRepository> mockMembershipRepo;
        private Mock<IRoleRepository> mockRoleRepo;
        private Mock<IMotoHubUserRepository> mockUserRepo;

        private EventsController eventsController;

        
        [SetUp]
        public void Setup()
        {
            events = new List<Event>()
            {
                new Event()
                {
                    EventsID = 8573,
                    ClubID = 10029,
                    MotoHubUserID = 73,
                    EventDate = new DateTime(2014, 5, 23),
                    EventName = "Stuffs Happening",
                    EventLocationStreet = "1474 Elo Street",
                    EventLocationCity = "Eugene",
                    EventLocationState = "OR",
                    EventLocationZip = 97461,
                    GalleryID = 44642
                }
            };

            clubs = new List<Club>()
            {
                new Club()
                {
                    ClubsID = 10029,
                    ClubName = "Test Club",
                    GalleryID = 223
                }
            };

            users = new List<MotoHubUser>()
            {
                new MotoHubUser()
                {
                    MotoHubUserID = 73,
                    AspNetIdentityID = "who knows"
                }
            };

            roles = new List<MotoHubRole>()
            {
                new MotoHubRole()
                {
                    RoleID = 1,
                    RoleTitle = "ClubAdmin"
                }
            };

            memberships = new List<Membership>()
            {
                new Membership()
                {
                    ClubID = 10029,
                    MotoHubUserID = 73,
                    RoleID = 1,
                    Status = true
                }
            };
            
        }
        

        [SetUp]


        [Test]
        public void Registration_CreatedSuccessfully_ReturnsTrueRegistrationIsCreated()
        {
            var controller = new ProfileController();
            Registration testClub = new Registration
            {
                EventID = 10,
                UserID = 23
            };

            var result = controller.CreatedSuccessfully(testClub);

            Assert.IsTrue(result);
        }
        /*
        [Test]
        public void Event_CreatedSuccessfully_ReturnsTrueOnEventSuccessfulCreation()
        {
            mockEventRepo = new Mock<IEventRepository>();

            mockEventRepo.Setup(m => m.GetAll()).Returns(events);

            eventsController = new EventsController(mockEventRepo.Object);

            bool result = eventsController.MoqCreatedSuccessfully();

            Assert.IsTrue(result);
        }
        */
        /// <summary>
        /// Test for task 286
        /// </summary>
        [Test]
        public void Event_CheckIfCurrentUserIsClubAdmin_ReturnsTrue()
        {
            mockEventRepo = new Mock<IEventRepository>();
            mockClubRepo = new Mock<IClubRepository>();
            mockUserRepo = new Mock<IMotoHubUserRepository>();
            mockMembershipRepo = new Mock<IMembershipRepository>();
            mockRoleRepo = new Mock<IRoleRepository>();

            mockEventRepo.Setup(e => e.GetAll()).Returns(events);
            mockClubRepo.Setup(c => c.GetAll()).Returns(clubs);
            mockMembershipRepo.Setup(m => m.GetAll()).Returns(memberships);
            mockRoleRepo.Setup(r => r.GetAll()).Returns(roles);
            mockUserRepo.Setup(u => u.GetAll()).Returns(users);

            eventsController = new EventsController(mockClubRepo.Object, mockEventRepo.Object, mockUserRepo.Object, mockMembershipRepo.Object, mockRoleRepo.Object);

            bool result = eventsController.TestCheckIfCurrentUserIsClubAdmin(8573, 10029, 73);

            Assert.IsTrue(result);
        }


    }
}
