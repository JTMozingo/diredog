﻿using System;
using TechTalk.SpecFlow;

namespace MotoHubTest
{
    [Binding]
    public class PBI348Steps
    {
        [Given(@"I have entered events in the year (.*)")]
        public void GivenIHaveEnteredEventsInTheYear(int p0)
        {
            ScenarioContext.Current.Pending();
        }
        
        [Given(@"I have entered an event")]
        public void GivenIHaveEnteredAnEvent()
        {
            ScenarioContext.Current.Pending();
        }
        
        [Given(@"the event Date has past")]
        public void GivenTheEventDateHasPast()
        {
            ScenarioContext.Current.Pending();
        }
        
        [When(@"I enter ""(.*)"" into the URL")]
        public void WhenIEnterIntoTheURL(string p0)
        {
            ScenarioContext.Current.Pending();
        }
        
        [Then(@"the ""(.*)"" chart will be visible\.")]
        public void ThenTheChartWillBeVisible_(string p0)
        {
            ScenarioContext.Current.Pending();
        }
        
        [Then(@"the ""(.*)"" chart will be visible")]
        public void ThenTheChartWillBeVisible(string p0)
        {
            ScenarioContext.Current.Pending();
        }
    }
}
