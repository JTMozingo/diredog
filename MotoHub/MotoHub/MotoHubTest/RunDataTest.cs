﻿using NUnit.Framework;
using MotoHub.Controllers;
using MotoHub.Models;
using Moq;
using System;
using MotoHub.Repositories.RepositoryInterfaces;
using System.Collections.Generic;
using System.Web.Mvc;
using MotoHub.Repositories;

namespace MotoHubTest
{
    [TestFixture]
    public class RunDataTest
    {
        private List<RunData> runs;

        private Mock<IRunDataRepository> mockRunRepo;

        private RunDataController runController;

        [SetUp]
        public void Setup()
        {
            runs = new List<RunData>()
            {
                new RunData()
                {
                    RunDataID = 789,
                    EventResultsID = 2,
                    RunNumber = 1,
                    RawTime = null,
                    Penalty = 0,
                    Finished = true,
                    AdjustedTime = null
                },
                new RunData()
                {
                    RunDataID = 790,
                    EventResultsID = 4,
                    RunNumber = 1,
                    RawTime = null,
                    Penalty = 0,
                    Finished = true,
                    AdjustedTime = null
                },
                new RunData()
                {
                    RunDataID = 792,
                    EventResultsID = 9,
                    RunNumber = 1,
                    RawTime = null,
                    Penalty = 0,
                    Finished = true,
                    AdjustedTime = null
                },
                new RunData()
                {
                    RunDataID = 791,
                    EventResultsID = 2,
                    RunNumber = 2,
                    RawTime = null,
                    Penalty = 0,
                    Finished = true,
                    AdjustedTime = null
                }
            };
        }

        [Test]
        public void RunData_TestGetNext_ReturnsNextItem()
        {
            RunData run = runs.Find(x => x.RunDataID == 791);
            //
        }
    }
}
