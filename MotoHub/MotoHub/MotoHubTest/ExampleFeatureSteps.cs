﻿using System;
using TechTalk.SpecFlow;
using OpenQA.Selenium;
using OpenQA.Selenium.Firefox;
using NUnit.Framework;

namespace MotoHubTest
{
    [Binding]
    public class ExampleFeatureSteps
    {
        private FirefoxDriver driver = new FirefoxDriver();

        [BeforeTestRun]
        public static void Setup()
        {
        }

        [Given(@"I have entered ""(.*)"" into the URL")]
        public void GivenIHaveEnteredIntoTheURL(string url)
        {
           driver.Manage().Timeouts().ImplicitWait.Duration().Add(TimeSpan.FromSeconds(10));
           driver.Navigate().GoToUrl(url);
        }
        
        [When(@"I press the ""(.*)"" button")]
        public void WhenIPressTheButton(string buttonID)
        {
            IWebElement button = driver.FindElementById(buttonID);

            button.Click();
        }
        
        [Then(@"the result should be ""(.*)"" in the URL")]
        public void ThenTheResultShouldBeInTheURL(string url)
        {
            string foundURL = driver.Url.ToString();

            Assert.AreEqual(foundURL, url);
        }
    }
}
