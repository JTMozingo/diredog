﻿Feature: PBI348
	In order to gain some insights on my race performance
	As a MotoHubUser
	I want to be able to view charts with my race time and point averages.

@mytag
Scenario: Show Average Points per Year
	Given I have entered events in the year 2016
	And I have entered events in the year 2017
	When I enter "http://localhost:56142/Profile/Home" into the URL
	Then the "Average Points Per Year" chart will be visible.

Scenario: Show Average Adjusted Time
	Given I have entered an event
	And the event Date has past
	When I enter "http://localhost:56142/Profile/Home" into the URL
	Then the "Average Adjusted Time" chart will be visible