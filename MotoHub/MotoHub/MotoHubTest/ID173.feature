﻿Feature: ID173
	In order to make registration and user look up simple,
	I want QR Codes to return information depending on which type of QR code was scanned.

Scenario: Visit QR Scan page
	Given I have entered "motohub.azurewebsites.net/EventRegistration/QRScan" into the URL
	When I press the blue "Choose File/Browse" button and take or upload a picture
	Then the result should be information from the Car or User encoded