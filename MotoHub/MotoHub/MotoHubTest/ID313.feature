﻿Feature: ID173
	In order to make registration and user look up simple,
	I want QR Codes to return information depending on which type of QR code was scanned.

Scenario: Navigate to Queue Page
	Given I have navigated to "motohub.azurewebsites.net/Events", and clicked on an Event of which I am an admin
	When I press the blue "Queue" button at the top of the page
	Then the result should be "motohub.azurewebsites.net/Events/Queue/#" in the URL

Scenario: Show user information
	Given I have navigated to "motohub.azurewebsites.net/Events/Queue/#"
	When I press the "Choose File" button, if the scan is sucessful
	Then the registration information for that QR Code should show on the screen

Scenario: Add user to Queue
	Given I have naviagated to "motohub.azurewebsites.net/Events/Queue/#", and uploaded a QR Code
	When I press the "Add" button
	Then new RunData entry should be added to the table.

Scenario: Navigate to timing
	Given that I have navigated to "motohub.azurewebsites.net/Events", and clicked on an Event of which I am an admin
	When I press the blue "Timing" button at the top of the page
	Then the result should be "motohub.azurewebsites.net/Events/Timing/#" in the URL

Scneario: Page updating
	Given that I have navigated to "motohub.azurewebsites.net/Events/Timing/#"
	When the database is updated
	Then the result should be edit entries for new Queue'd users