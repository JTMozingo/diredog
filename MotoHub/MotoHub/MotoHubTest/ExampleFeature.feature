﻿Feature: ExampleFeature
	In order to visit the MotoHub site
	As a user of the internet
	I want to be able to navigate to the homepage with the URL bar.

@mytag
Scenario: Visit the homepage
	Given I have entered "http://localhost:56142/" into the URL
	When I press the "registerLink" button
	Then the result should be "http://localhost:56142/Account/Register" in the URL



	