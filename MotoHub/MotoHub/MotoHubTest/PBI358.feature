﻿Feature: PBI358
	In order to avoid making MotoHubUsers mad by adding them to clubs they have not specifically asked to be added to
	As a MotoHub Club Admin
	I want to be able to invite users to my club

@mytag
Scenario: Invite a user to a club without being allowed to invite them again (spam invites).
	Given I have logged into the club admin page for a club
	And I have clicked "id='add-membership-button'"
	When I exit the window
	And I click "id='add-membership-button'" again
	Then the user I invited should not be in the list.

Scenario: Accept a club invitation
	Given I have logged into my account
	And I have clicked "id='toggle-invitation-modal'"
	When I click "//button[@onclick='acceptClubInvitation(event, 3)']"
	Then the club should be added to 'My Clubs'
