# DireDog 
## Team Project
### Project Ideas

A short ouline of 3 project ideas for the team of Alex LeClerc, JT Mozingo, and TJ Dennis for the CS 461 Spring term Software Engineering Class.
The basic outline of each project is that it has to be implemented using C# development on Visual Studios IDE and implemented using the Azure web services. In side of each project there must be some implementation of an API from a known source and that the project be large enough in scope that it cannot be finished in mere days. 

EDIT: added more information and  Q&A 

### Idea 1: Party App

For the first idea we considered creating an application that will allow a group of people to share and add to a music playlist so that only 1 device has to stream the audio to a hardware device.
A typical use case would be to have the main user to have a spotify (or similar) account, then those who were invited into the group can make suggestions on what to play next and to build up a queue without having to switch devices. This would allow for anyone to choose the music.
    
The biggest concern here is to get the playlist to coordinate between multiple inputs so that everyone would be working with the most up to date version. 
Local connectivity would be quicker but less reliable and needs people to open up their phones with either bluetooth or similar. Going with a web interface would help as long as the user has a stable connection. Another consideration is how to access the music. 
To work with a spotify API seems like it will be simple, there are tutiorals of how to integrate it with your current applications. 
Another aspect of the application could be to make a game of how the play list goes, 
    An upvote on a song you chose could earn you points, those points could be used to shuffle the list, skip a song, or to switch out a song for a parody version instead.


What is new/original about this idea? What are related websites/apps? (Be able to answer the question: isn’t somebody already doing this?)
    This idea allows for multiple people to join together a single playlist operating on a single device without needing to sync up the local devices on the same network.
    
Why is this idea worth doing? Why is it useful and not boring?
    Having an application to manage the atmosphere of a room (and possibly turn it into a game) with the point system can help to provide a new aspect of how a user interacts with their music.
    
What resources will be required for you to complete this project that are not already included in the class. i.e. you already have the Microsoft stack, server, database so what else would you need? Additional API’s, frameworks or platforms you’ll need to use.
    A spotify API would be the easiest way to integrate our idea along with a frame work on mobile devices such that a cloud or azure based site can track the playlist, points, and users to interface with each individual.
    
What algorithmic content is there in this project? i.e. what algorithm(s) will you have to develop or implement in order to do something central to your project idea? (Remember, this isn’t just a software engineering course, it is your CS degree capstone course!)
    Having the point system for each user will require some algorithm as we will need to determine if they have enough points to change the playlist, along with how those changes may include variations on songs or cover bands.
    
Rate the topic with a difficulty rating of 1-10. One being supremely easy to implement (not necessarily short though). Ten would require the best CS students using lots of what they learned in their CS degree, plus additional independent learning, to complete successfully.
    Using a new API is never easy, along with mobile development that has never been covered in the required courses this project would be among the 7-8 range in difficulty. The new platform integrations will be the biggest challenge.
    
#### Idea 2: Security System
Edit: Cut out from potential project ideas

With the ease of access with the cloud and with AI devices, we thought of integrating a home based system with our phones so that we can control and monitor our homes remotely.
Since the infrastructure of Alexa, Google, and other systems that allow you to extend their software the consideration is that you dont have to buy into a security package that costs a recurring bill each month and instead monitor everything on your own terms. 
    
### Idea 3: Event Organizer

Most events dont have a platform that can be tailored to their needs and desires. Larger events such as concerts and graduations have to be largely self organized and making sure that everyone is on the same page at the same time is nearly impossible.
An application that is designed to work with the needs of an organizer such that everyone involved can quickly provide feedback, information, and RSVP would help. 
    
We decided that a generic and abstract approach to the design template (instead of just it being for one type of event) would be the most valuable. The user can then specify how the application can be tailored to their needs, such as if they need certain access to database tools. 
The end user wont know that they need a database but just a page to keep track of users or entries or the like. Going from the abstract view to begin with brings in its own challenges. We might have to visualize this with a specific model so that we dont lose how everything is related. 

What is new/original about this idea? What are related websites/apps? (Be able to answer the question: isn’t somebody already doing this?)
    There are similar applications but usually they are tailored to a specific event or group. The structure for this project would be to create an abstract model that would then have a 'admin' style user provide the basic inputs of how they would like their app to operate. Related websites would be social networking groups, calendar planners, and REC*It that WOU uses.
    
Why is this idea worth doing? Why is it useful and not boring?
    This idea is marketable to many organizations that would like to have a custom tailored app but cannot afford to build one from the ground up. We would design everything from the database to the algorithms needed and all that the end user would have to provide is the sample space that would be tailed to with minimal developer interaction.
    
What resources will be required for you to complete this project that are not already included in the class. i.e. you already have the Microsoft stack, server, database so what else would you need? Additional API’s, frameworks or platforms you’ll need to use.
    The biggest resource will be the scaffolding structure set that is implied with the modelling. We will need to implement a way for abstract data types to interact in such a way that it will be near unbreakable by those who want to take pieces that work well with their organizations idea.
    
What algorithmic content is there in this project? i.e. what algorithm(s) will you have to develop or implement in order to do something central to your project idea? (Remember, this isn’t just a software engineering course, it is your CS degree capstone course!)
    Since most of the design is algorithm based, the biggest that could be offered is generic data handling in the form of pre-set models that could take inputs defined by the user, have the user designate what they would like the data to do, and then run autonomously.
    
Rate the topic with a difficulty rating of 1-10. One being supremely easy to implement (not necessarily short though). Ten would require the best CS students using lots of what they learned in their CS degree, plus additional independent learning, to complete successfully.
    Considering the hurdles with how much abstract modelling and the unknown quantities involved in having an end user design their own application with essentially building blocks, This project would rate about and 8-9.