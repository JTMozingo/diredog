﻿--CS 461 Winter term SQL Up script for the class project
--Project centers around a fitness tracker application and building
--a website that allows for data analysis



CREATE TABLE dbo.Coach
(
	ID Int IDENTITY(1,1) NOT NULL,
	FirstName	VARCHAR(64) NOT NULL,
	LastName	VARCHAR(64) NOT NULL,
	IsHeadCoach BIT,
	IsActive	BIT,

	CONSTRAINT [PK_dbo.Coach] PRIMARY KEY CLUSTERED (ID ASC)
	);

	CREATE TABLE dbo.Team
(
	ID			INT IDENTITY (1,1) NOT NULL,
	TeamName	NVARCHAR(64) NOT NULL,
	HeadCoach	INT NOT NULL,
	
	CONSTRAINT [PK_dbo.Team] PRIMARY KEY CLUSTERED (ID ASC),
	Constraint [FK_dbo.HeadCoach] Foreign Key (HeadCoach)
		references dbo.Coach(ID)
		on delete Cascade
		on update cascade
);

CREATE TABLE dbo.Athlete
(
	ID				INT IDENTITY (1,1) NOT NULL,
	
	FirstName		NVARCHAR(64) NOT NULL,
	LastName		NVARCHAR(64) NOT NULL,
	Age				NVARCHAR(64),
	Squad			INT,
	
	CONSTRAINT [PK_dbo.Artists] PRIMARY KEY CLUSTERED (ID ASC),
	Constraint [FK_dbo.Team] Foreign Key (Squad)
		references dbo.Team(ID)
		on delete cascade
		on update cascade
);

CREATE TABLE dbo.RunData (
	RunID			INT IDENTITY(1,1) NOT NULL,
	AthleteID		INT NOT NULL,
	Distance		DECIMAL,
	RunTime			DECIMAL,
	HeartRate		DECIMAL,
	StepsPMin		DECIMAL,

	CONSTRAINT[PK_dbo.RunData] PRIMARY KEY CLUSTERED (RunID ASC),
	CONSTRAINT[FK_dbo.AthleteID] FOREIGN KEY (AthleteID)
		REFERENCES dbo.Athlete (ID)
		ON DELETE CASCADE
		ON UPDATE CASCADE,
);


Insert Into dbo.Coach(FirstName, LastName, IsHeadCoach, IsActive) Values
	('Dave', 'Holmes','1','1'),
	('Andrew','Helm', '1','1'),
	('Amanda', 'Button','1','1'),
	('Hayley', 'Osborn','1','1'),
	('Martin', 'Gonzalez','0','0');

Insert INTO dbo.Team (TeamName, HeadCoach) Values
	('Varsity Boys','1'),
	('Junior Varsity Boys','2'),
	('Varsity Girls','3'),
	('Junior Varsity Girls', '4'),
	('Reserves','2');

Insert INTO dbo.Athlete (FirstName, LastName, Age, Squad) Values
	('Kyle', 'Wishram', '16','1'),
	('Jason', 'Daugherty', '17','1'),
	('Jordan', 'Gibson', '18','1'),
	('Joshua', 'Ringle', '16','1'),
	('Jesus', 'DeRosa', '17','1'),
	('Matthew', 'Sheldon', '17','1'),
	('Jake', 'Morris', '15','1'),


	('Emily', 'Wishram', '16','3'),
	('Katie', 'Horn', '17','3'),
	('Lillie', 'Blaire', '15','3'),
	('Katie', 'Ringo', '15','3'),
	('Amanda', 'Belle', '17','3'),
	('Suzanne', 'Horn', '15','3'),
	('Elizabeth', 'Gates', '16','3');
	
INSERT INTO dbo.RunData (AthleteID, Distance, RunTime, HeartRate, StepsPMin) VALUES
	('2', '13.44', '106.3', '120', '140'),
	('2', '5.95', '66.8', '134', '182'),
	('7', '8.52', '80.2', '126', '172'),
	('7', '18.57', '112.4', '140', '185'),
	('13', '10.32', '101.5', '154','189'),
	('10', '9.78', '91.3', '160', '178'),
	('10', '12.81', '118.9', '157', '174'),
	('10', '10.25', '104.3', '159', '179');

	GO