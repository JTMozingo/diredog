﻿/*
Credit for this wonderful plugin goes to https://github.com/weixiyen/jquery-filedrop
*/

$(function () {
    $('#drop-box').filedrop({
        url: '/Users/FileUpload',
        allowedfileextensions: ['.FIT'],
        paramname: 'selectedFiles',
        maxfiles: 5,
        maxfilesize: 5, // in MB
        dragOver: function () {
            $('#drop-box').addClass('active-drop');
        },
        dragLeave: function () {
            $('#drop-box').removeClass('active-drop');
        },
        drop: function () {
            $('#drop-box').removeClass('active-drop');
        },
        afterAll: function (e) {
            $('#drop-box').html('File(s) uploaded successfully.');
        },
        uploadFinished: function (i, file, response, time) {
            $('#uploadList').append('<li class="list-group-item">' + file.name + '</li>')
        }
    })
})